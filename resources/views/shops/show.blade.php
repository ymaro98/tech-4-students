@extends('layouts.app')

@section('title', str_ireplace(["[webshop]"], [$webshop->name], $page->title))
@section('meta_description', str_ireplace(["[webshop]"], [$webshop->name], $page->meta_description))
@section('meta_keywords', str_ireplace(["[webshop]"], [$webshop->name], $page->meta_keywords))

@section('content')
<div id="app">

    <div class="container">
        <h1>{{ $webshop->name }}</h1>
        <webshop-products webshop_id="{{ $webshop->id }}" webshop-slug="{{ $webshop->name_slug }}"></webshop-products>
        <go-to-offers></go-to-offers>
        <btf-button></btf-button>
    </div>
</div>
@endsection
