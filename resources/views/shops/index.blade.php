@extends('layouts.app')

@section('title', $page->title)
@section('meta_description', $page->meta_description)
@section('meta_keywords', $page->meta_keywords)

@section('content')
    <div id="app">

        <div class="container">
            <div class="card my-2 p-3">
                <h2 class="text-center mt-2">{{ $page->excerpt }} <span class="badge badge-secondary text-dark t-bg-yellow">{{ $webshop_count }}</span></h2>
                <div class="mt-1 mx-md-5 text-center text-md-left">
                    {!! $page->body !!}
                </div>
            </div>
            <webshops></webshops>
        </div>
    </div>
@endsection
