@extends('voyager::master')

@section('css')
<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.2.2/css/buttons.bootstrap.min.css">
<link href="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap4.min.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/select/1.2.7/css/select.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/jszip-2.5.0/b-1.6.2/b-html5-1.6.2/datatables.min.css"/>
@stop

@section('content')
<div class="container">

    <table class="table table-bordered" style="width:100%">
        <thead>
            <tr>
                <th>#</th>
                <th>shop</th>
                <th>name</th>
                <th>brand</th>
                <th>os</th>
                <th>resolution</th>
                <th>screen_size</th>
                <th>cpu_family</th>
                <th>cpu_model</th>
                <th>cpu_freq</th>
                <th>gpu_family</th>
                <th>gpu_model</th>
                <th>ram</th>
                <th>storage</th>
                <th>color</th>
                <th>image</th>
                {{-- <th>price</th> --}}
                <th>url</th>
                <th>Created_at</th>
            </tr>
        </thead>
        <tbody>
            @php
                $x = 1;
            @endphp
            @foreach($data as $row)
            <tr>
                <td>{{$x}}</td>
                <td>{{$row->webshop}}</td>
                <td>{{$row->name}}</td>
                <td>{{$row->brand}}</td>
                <td>{{$row->system}}</td>
                <td>{{$row->resolution}}</td>
                <td>{{$row->screen_size}}</td>
                <td>{{$row->cpu_fam}}</td>
                <td>{{$row->cpu_model}}</td>
                <td>{{$row->cpu_ghz}}</td>
                <td>{{$row->gpu_fam}}</td>
                <td>{{$row->gpu_model}}</td>
                <td>{{$row->ram}}</td>
                <td>{{$row->storage_size}}</td>
                <td>{{$row->color}}</td>
                <td>
                    <img class="img-fluid" style="width:100px;" src="{{asset("storage/".$row->image)}}">
                    <span style="display:none;">{{asset("storage/".$row->image)}}</span>
                </td>
                {{-- @php
                    $price = json_decode($row->price);
                @endphp
                <td>
                    @foreach($price as $key=>$p)
                        <span>{{$p?$key.":".$p : ""}}</span>
                    @endforeach
                </td> --}}
                <td><a href="{{$row->url}}" target="_blank">{{$row->url}}</a></td>
                <th>{{$row->created_at}}</th>
            </tr>
            @php
                $x++;
            @endphp
            @endforeach
        </tbody>
    </table>
</div>

<!-- Datatables -->
<script src="https://code.jquery.com/jquery-3.5.1.js" integrity="sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc=" crossorigin="anonymous"></script>
<script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.21/js/dataTables.bootstrap4.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.bootstrap4.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.colVis.min.js"></script>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/v/bs4/jszip-2.5.0/b-1.6.2/b-html5-1.6.2/datatables.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>

<script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
<script src="{{asset("js/jszip.min.js")}}"></script>
<script>
var table = $('table').DataTable({
    dom:"lBftip",
    responsive:true,
    select:"single",
    buttons:[
        "print",
        "copy",
        "csv",
        "excel"
    ],
    "columnDefs":[{
        "visible":false,
        "targets":[3]
    }]
});
</script>
@stop

