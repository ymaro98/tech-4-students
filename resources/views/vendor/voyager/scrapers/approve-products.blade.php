@extends('voyager::master')

@section('css')
    <style>

        .outer table, th, td{
            /* border: 1px solid black; */
        }

        .outer th, td {
            padding: 5px;
        }

        .outer td element {
            width: 20%;
        }

        .outer .table {
            width: 100%;
            margin-left:20px
        }

        .outer{
            width: calc(100% - 100px);
            margin-left: 50px;
            margin-top: 25px;
            margin-bottom: 25px;
        }

        .outer .row {
            margin-right: 0 !important;
            margin-left: 0 !important;
        }

        .outer .header {
            /* border: 1px solid black; */
            /* border-top: 1px solid black; */
            padding: 10px;
            padding-left: 15px;
            padding-top: 18px;
            /* color: black; */
            background-color: #efefef;
        }

        .outer .border-top{
            border-top: 1px solid black;
            border-left: 1px solid black;
            border-right: 1px solid black;
        }

        .outer .border-bottom{
            border-bottom: 1px solid black;
            border-left: 1px solid black;
            border-right: 1px solid black;
        }

        .outer .content{
            /* border: 1px solid black; */
            background-color: #efefef;
        }

        .outer .type{
            float: right;
        }

        .outer .type-aanbieding{
            float:right;
        }

        .outer a {
            color: black;
            text-decoration: underline;
        }

        #approveForm{
            width: 45%;
            float: left;
        }
        #tempProduct{
            width: 45%;
            float: right;
            height: 1500px;
        }
        #prod-url, .form-control.image{
            height: max-content;
        }
    </style>
@stop

@section('content')
    <link href="{{ asset('css/product-card.css') }}" rel="stylesheet">
    <div style="background-size:cover; background-image: url({{ Voyager::image( Voyager::setting('admin.bg_image'), voyager_asset('/images/bg.jpg')) }}); background-position: center center;position:absolute; top:0; left:0; width:100%; height:300px;"></div>
    <div style="height:250px; display:block; width:100%"></div>
    <div class="container">
        @if($temp_product)
            <h2 class="text-center">Approve Products</h2>
            <h3 class="text-center">{{ $productsToApprove }} Products to Approve</h3>
            <div style="height:20px; display:block; width:100%"></div>
            <hr>

            <div id="approveForm">
                <h2 class="text-center">Approve Form</h2>
                <form id="approve-item-form" action="{{ route('save.approved.item') }}" method="POST">
                    @csrf
                    <div class="form-group">
                        <label for="webshops">Name</label>
                        <input class="form-control" type="text" name="name" id="name" value="{{ $temp_product->name }}" placeholder="Edit value...">
                        <input class="form-control" type="hidden" name="name_original" id="name_original" value="{{ $temp_product->name }}" placeholder="Edit value...">

                        <label for="webshops">Name Slug</label>
                        <input class="form-control" type="text" name="name_slug" id="name_slug" value="{{ $temp_product->name_slug }}" placeholder="Edit value...">

                        <label for="webshops">Webshop</label>
                        <select class="form-control" name="webshops" id="webshops">
                            <x-similar-option :options="$options->webshops" />
                        </select>
                        <input class="form-control" type="text" name="webshops_rep" id="webshops_rep" placeholder="Edit value...">

                        <label for="systems">System</label>
                        <select class="form-control" name="systems" id="systems">
                            <x-similar-option :options="$options->systems" />
                        </select>
                        <input class="form-control" type="text" name="systems_rep" id="systems_rep" placeholder="Edit value...">

                        <label for="brands">Brand</label>
                        <select class="form-control" name="brands" id="brands">
                            <x-similar-option :options="$options->brands" />
                        </select>
                        <input class="form-control" type="text" name="brands_rep" id="brands_rep" placeholder="Edit value...">

                        <label for="resolutions">Resolution</label>
                        <select class="form-control" name="resolutions" id="resolutions">
                            <x-similar-option :options="$options->resolutions" />
                        </select>
                        <input class="form-control" type="text" name="resolutions_rep" id="resolutions_rep" placeholder="Edit value...">

                        <label for="resolutions">Screen Size</label>
                        <select class="form-control" name="screen_sizes" id="screen_sizes">
                            <x-similar-option :options="$options->screen_sizes" />
                        </select>
                        <input class="form-control" type="text" name="screen_sizes_rep" id="screen_sizes_rep" placeholder="Edit value...">

                        <label for="cpu_fams">CPU Family</label>
                        <select class="form-control" name="cpu_fams" id="cpu_fams">
                            <x-similar-option :options="$options->cpu_fams" />
                        </select>
                        <input class="form-control" type="text" name="cpu_fams_rep" id="cpu_fams_rep" placeholder="Edit value...">

                        <label for="cpu_models">CPU Model</label>
                        <select class="form-control" name="cpu_models" id="cpu_models">
                            <x-similar-option :options="$options->cpu_models" />
                        </select>
                        <input class="form-control" type="text" name="cpu_models_rep" id="cpu_models_rep" placeholder="Edit value...">

                        <label for="cpu_freqs">CPU Frequency</label>
                        <select class="form-control" name="cpu_freqs" id="cpu_freqs">
                            <x-similar-option :options="$options->cpu_freqs" />
                        </select>
                        <input class="form-control" type="text" name="cpu_freqs_rep" id="cpu_freqs_rep" placeholder="Edit value...">

                        <label for="gpu_fams">GPU Family</label>
                        <select class="form-control" name="gpu_fams" id="gpu_fams">
                            <x-similar-option :options="$options->gpu_fams" />
                        </select>
                        <input class="form-control" type="text" name="gpu_fams_rep" id="gpu_fams_rep" placeholder="Edit value...">

                        <label for="gpu_models">GPU Model</label>
                        <select class="form-control" name="gpu_models" id="gpu_models">
                            <x-similar-option :options="$options->gpu_models" />
                        </select>
                        <input class="form-control" type="text" name="gpu_models_rep" id="gpu_models_rep" placeholder="Edit value...">

                        <label for="rams">Ram</label>
                        <select class="form-control" name="rams" id="rams">
                            <x-similar-option :options="$options->rams" />
                        </select>
                        <input class="form-control" type="text" name="rams_rep" id="rams_rep" placeholder="Edit value...">

                        <label for="storage_sizes">Storage Size</label>
                        <select class="form-control" name="storage_sizes" id="storage_sizes">
                            <x-similar-option :options="$options->storage_sizes" />
                        </select>
                        <input class="form-control" type="text" name="storage_sizes_rep" id="storage_sizes_rep" placeholder="Edit value...">

                        <label for="colors">Color</label>
                        <select class="form-control" name="colors" id="colors">
                            <x-similar-option :options="$options->colors" />
                        </select>
                        <input class="form-control" type="text" name="colors_rep" id="colors_rep" placeholder="Edit value...">

                        <label for="colors">Image</label>
                        <input class="form-control" type="text" name="image" id="image" value="{{ $temp_product->image }}">

                        <label for="colors">Url</label>
                        <input class="form-control" type="text" name="url" id="url" value="{{ $temp_product->url }}">
                    </div>

                    <button type="submit" class="btn btn-block btn-primary">Approve Item</button>
                </form>
            </div>
            <div id="tempProduct">

                <h2 class="text-center">Product to Approve</h2>

                <p>Name</p>
                <h2 class="form-control">{{ $temp_product->name }}</h2>

                <p>Name Slug</p>
                <h2 class="form-control">{{ $temp_product->name_slug }}</h2>

                <p>Webshop</p>
                <h2 class="form-control">{{ $temp_product->webshop }}</h2>

                <p>System</p>
                <h2 class="form-control">{{ $temp_product->system }}</h2>

                <p>Brand</p>
                <h2 class="form-control">{{ $temp_product->brand }}</h2>

                <p>Resolution</p>
                <h2 class="form-control">{{ $temp_product->resolution }}</h2>

                <p>Screen Size</p>
                <h2 class="form-control">{{ $temp_product->screen_size }}</h2>

                <p>CPU Family</p>
                <h2 class="form-control">{{ $temp_product->cpu_fam }}</h2>

                <p>CPU Model</p>
                <h2 class="form-control">{{ $temp_product->cpu_model }}</h2>

                <p>CPU Frequency</p>
                <h2 class="form-control">{{ $temp_product->cpu_ghz }}</h2>

                <p>GPU Family</p>
                <h2 class="form-control">{{ $temp_product->gpu_fam }}</h2>

                <p>GPU Model</p>
                <h2 class="form-control">{{ $temp_product->gpu_model }}</h2>

                <p>Ram</p>
                <h2 class="form-control">{{ $temp_product->ram }}</h2>

                <p>Storage Size</p>
                <h2 class="form-control">{{ $temp_product->storage_size }}</h2>

                <p>Color</p>
                <h2 class="form-control">{{ $temp_product->color }}</h2>

                <p>Image</p>
                <h2 class="form-control image">{{ $temp_product->image }}</h2>

                <p>Url</p>
                <h2 id="prod-url" class="form-control">{{ $temp_product->url }}</h2>
                <button id="go-to-site" class="btn form-control">Ga naar site <span class="icon voyager-forward"></span></button>
            </div>
            {{-- <iframe id="tempProduct" src="{{url($temp_product->url)}}">Your browser isn't compatible</iframe> --}}
        @else
            <h2 class="text-center">No products to approve</h2>
        @endif
    </div>
    <script>
        const link_btn = document.getElementById('go-to-site');
        link_btn.addEventListener('click', (e) => {
            const url = document.getElementById('prod-url');
            
            let location = url.innerHTML;
            window.open(location, '_blank');
        })
    </script>
@stop

