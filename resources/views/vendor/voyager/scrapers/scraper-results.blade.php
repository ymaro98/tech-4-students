@extends('voyager::master')

@section('css')
    <style>

        .outer table, th, td{
            /* border: 1px solid black; */
        }

        .outer th, td {
            padding: 5px;
        }

        .outer td element {
            width: 20%;
        }

        .outer .table {
            width: 100%;
            margin-left:20px
        }

        .outer{
            width: calc(100% - 100px);
            margin-left: 50px;
            margin-top: 25px;
            margin-bottom: 25px;
        }

        .outer .row {
            margin-right: 0 !important;
            margin-left: 0 !important;
        }

        .outer .header {
            /* border: 1px solid black; */
            /* border-top: 1px solid black; */
            padding: 10px;
            padding-left: 15px;
            padding-top: 18px;
            /* color: black; */
            background-color: #efefef;
        }

        .outer .border-top{
            border-top: 1px solid black;
            border-left: 1px solid black;
            border-right: 1px solid black;
        }

        .outer .border-bottom{
            border-bottom: 1px solid black;
            border-left: 1px solid black;
            border-right: 1px solid black;
        }

        .outer .content{
            /* border: 1px solid black; */
            background-color: #efefef;
        }

        .outer .type{
            float: right;
        }

        .outer .type-aanbieding{
            float:right;
        }

        .outer a {
            color: black;
            text-decoration: underline;
        }
    </style>
@stop

@section('content')
    <link href="{{ asset('css/product-card.css') }}" rel="stylesheet">
    <div style="background-size:cover; background-image: url({{ Voyager::image( Voyager::setting('admin.bg_image'), voyager_asset('/images/bg.jpg')) }}); background-position: center center;position:absolute; top:0; left:0; width:100%; height:300px;"></div>
    <div style="height:250px; display:block; width:100%"></div>
    <div class="container">
        <h2 class="text-center">Webshop Scrapers</h2>
        <div style="height:20px; display:block; width:100%"></div>

        <div class="results row">
            @foreach ($scraperResults as $result)

                <div class='outer'>
                    <div class="row header" style="border-bottom: 5px solid {{ $result->status_kleur }};">

                        <div class="col-md-9 name-maat">
                            <strong><h4>{{ $result->name }}</h4></strong>
                        </div>
                        <div class="type col-md-3" >
                           <strong><h4 class='type-aanbieding'>{{ $result->status_type }}</h4></strong>
                        </div>
                    </div>

                    <div class="row">
                        <div class="content ">
                            <table style="width:100%; margin-left:20px;">
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <th></th>
                                    <th>Oude gegevens</th>
                                    <th>Gescrapte gegevens</th>
                                    <th></th>
                                </tr>
                                <tr>
                                    <td>Vanprijs</td>
                                    <td>{{ $result->vanprijs }}</td>
                                    <td>{{ $result->scraper_vanprijs }}</td>
                                    <td><a href="{{ $result->url }}" target="_blank" rel="nofollow"><strong>Aanbieding Webshop</strong></a></td>
                                </tr>
                                <tr>
                                    <td>Voorprijs</td>
                                    <td>{{ $result->voorprijs }}</td>
                                    <td>{{ $result->scraper_voorprijs }}</td>
                                    <td><a href="/WB/webshop-products/{{ $result->id }}/edit" target="_blank" rel="nofollow"><strong>Aanbieding WB</strong></a></td>
                                </tr>
                                <tr>
                                    <td>Actief</td>
                                    <td>{{ $result->active }}</td>
                                    <td>{{ $result->scraper_active }}</td>
                                    <td><a href="/webshop/{{ $result->shop }}/{{ $result->page }}/{{ $result->product->name_slug }}" target="_blank" rel="nofollow"><strong>Aanbieding eigen pagina</strong></a></td>
                                </tr>
                                <tr>
                                    <td>Actie</td>
                                    <td>Test</td>
                                    <td></td>
                                    <td>Aanbieding_ID: {{ $result->id }}</td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
@stop

