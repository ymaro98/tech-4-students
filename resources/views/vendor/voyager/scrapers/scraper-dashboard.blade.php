@extends('voyager::master')

@section('css')
    <style>

        .card-product {
            height: 510px!important;
        }
        .card-title {
            overflow: hidden;
            text-overflow: ellipsis;
            display: -webkit-box;
            -webkit-box-orient: vertical;
            -webkit-line-clamp: 2;
        }

        .card-product .img-wrap {
            border-radius: 3px 3px 0 0;
            overflow: hidden;
            position: relative;
            height: 220px;
            text-align: center;
        }

        .card-product .img-wrap img {
            max-height: 100%;
            max-width: 100%;
            -o-object-fit: cover;
            object-fit: cover;
        }

        .card-product .info-wrap {
            overflow: hidden;
            padding: 15px;
            border-top: 1px solid #eee;
        }

        .card-product .bottom-wrap {
            height: 180px;
            padding: 15px;
            border-top: 1px solid #eee;
        }

        .card-product .price-old {
            color: #999;
        }
    </style>
@stop

@section('content')
    <link href="{{ asset('css/product-card.css') }}" rel="stylesheet">
    <div style="background-size:cover; background-image: url({{ Voyager::image( Voyager::setting('admin.bg_image'), voyager_asset('/images/bg.jpg')) }}); background-position: center center;position:absolute; top:0; left:0; width:100%; height:300px;"></div>
    <div style="height:250px; display:block; width:100%"></div>
    <div class="container">
        <h2 class="text-center">Webshop Scrapers</h2>
        <div style="height:20px; display:block; width:100%"></div>

        @foreach ($webshops as $shop)
            <div class="col-md-4">
                <figure class="card card-product">
                    <div class="img-wrap"><img src="/storage/{{$shop->image}}"></div>
                    <figcaption class="info-wrap">
                        <h4 class="title card-title text-center">{{ $shop->name }}</h4>
                    </figcaption>
                    <div class="bottom-wrap">
                        <h2 class="text-center">Scraper Options</h2>
                        <a href="/WB/scraper/update-items/{{$shop->name_slug}}" class="btn btn-block btn-primary float-right" target="_blank">Update products</a>
                        <a href="/WB/scraper/new-items/{{$shop->name_slug}}" class="btn btn-block btn-primary float-right" target="_blank">Check new products</a>
                        <a href="/WB/scraper/approve-items/{{$shop->name_slug}}" class="btn btn-block btn-primary float-right" target="_blank">Approve new products</a>
                    </div>
                </figure>
                <div style="height:10px; display:block; width:100%"></div>
            </div>
        @endforeach
    </div>
@stop

