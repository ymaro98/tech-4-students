<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="@yield('meta_description')">
    <meta name="keywords" content="@yield('meta_keywords')">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <script>window.laravel = {csrfToken: '{{ csrf_token() }}'}</script>

    <title>@yield('title') | {{ setting('site.title') }}</title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ mix('css/app.css') }}" rel="stylesheet">
    
    <!-- Styles -->
    @yield('extra_styles')

    <!-- favicon -->
    <link rel="apple-touch-icon" sizes="180x180" href="/storage/logo/favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/storage/logo/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/storage/logo/favicon/favicon-16x16.png">
    <link rel="manifest" href="/storage/logo/favicon/site.webmanifest">
    <link rel="mask-icon" href="/storage/logo/favicon/safari-pinned-tab.svg" color="#5bbad5">
    <link rel="shortcut icon" href="/storage/logo/favicon/favicon.ico">
    <meta name="apple-mobile-web-app-title" content="Tech 4 Students">
    <meta name="application-name" content="Tech 4 Students">
    <meta name="msapplication-TileColor" content="#ffc40d">
    <meta name="msapplication-config" content="/storage/logo/favicon/browserconfig.xml">
    <meta name="theme-color" content="#ffc40d">

    <!-- TradeDoubler site verification 3175473 -->
</head>
<body>
    <div id="app">
        @if(setting('site.announcement_on_off') == 1)
            <div class="top-alert row justify-content-center">
                <h5 class="top-alert-text">
                    {{ setting('site.announcement_bar_text') }}
                </h5>
            </div>
        @endif
        <nav class="navbar navbar-expand-lg navbar-light shadow-sm sticky-top">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/') }}">
                    <div class="nav-image brand-logo">
                        <img class="logo-image" src="{{ asset('storage/logo/emblem_normal.png') }}" alt="{{ config('app.name', 'Laravel') }}">
                        <img class="logo-text" src="{{ asset('storage/logo/text_normal.png') }}" alt="{{ config('app.name', 'Laravel') }}">
                    </div>
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto text-center">

                        {{ menu('main', 'partials.menus.main_menu') }}
                    </ul>
                </div>
			</div>
        </nav>

        <main class="{{ (!\Request::is('/') && !\Request::is('artikelen')) ? 'py-4': '' }}">
            @yield('content')
            @auth
            <edit-page url='/WB/pages/{{ $page->id }}/edit'></edit-page>
            @endauth
        </main>
    </div>

    <x-footer>
    </x-footer>

    <!-- Scripts -->
    <script src="{{ mix('js/app.js') }}" defer></script>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-156486562-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());
        gtag('config', 'UA-156486562-1');
    </script>
</body>
</html>
