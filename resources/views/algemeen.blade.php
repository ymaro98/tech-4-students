@extends('layouts.app')

@section('title', $page->title)
@section('meta_description', $page->meta_description)
@section('meta_keywords', $page->meta_keywords)

@section('content')
    <div class="jumbotron jumbotron-default mt-2">
        <div class="container">
            <h1 class="display-md-3 text-center">{{ $page->excerpt }}</h1>
            <hr class="my-2">
            <p class="lead">
                {!! $page->body !!}
            </p>
            <hr class="my-2">
        </div>
    </div>
@endsection
