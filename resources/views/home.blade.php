@extends('layouts.app')

@section('title', $page->title)
@section('meta_description', $page->meta_description)
@section('meta_keywords', $page->meta_keywords)

@section('extra_styles')

@php
    $bg = 'https://images.unsplash.com/photo-1519389950473-47ba0277781c?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1000&q=80';
@endphp
<style>
    #hero-home-overlay{
        position: relative;
        display: block;
        width: 100%;
        min-height: 500px;
        padding: 5rem 2rem;
        background-color: rgba(0,0,0,0.5);
        z-index: 0;
    }

    #hero-home{
        background-image: url({{ $bg }});
        /* Set a specific height */
        min-height: 500px;

        /* Create the parallax scrolling effect */
        background-attachment: fixed;
        background-position: center;
        background-repeat: no-repeat;
        background-size: cover;
    }

    #articles-home-overlay{
        position: relative;
        display: block;
        width: 100%;
        min-height: 500px;
        padding: 5rem 2rem;
        background-color: rgba(0,0,0,0.5);
        z-index: 0;
    }
    
    #articles-home{
        background-image: url({{ $bg }});
        
        /* Set a specific height */
        min-height: 500px;

        /* Create the parallax scrolling effect */
        background-attachment: fixed;
        background-position: center;
        background-repeat: no-repeat;
        background-size: cover;
    }

    #top-offers{
        padding: 5rem 2rem; 
    }
</style>
@endsection('extra_styles')

@section('content')

    <div id="app">
        
        <div id="hero-home">
            <div id="hero-home-overlay">
                <div class="container">

                    <h1 class="display-4 display-md-3 text-center text-light">{{ setting('site.title') }}</h1>
                    <p class="lead text-center text-light">{{ $page->excerpt }}</p>
                    <hr class="my-2">
                    <p class="lead row justify-content-center">
                        <a class="t-btn s-lg d-bl standard col-md-4 mx-md-1 mb-2 mt-2" href="/aanbiedingen" role="button">Bekijk de beschikbare aanbiedingen</a>
                        <a class="t-btn s-lg d-bl standard col-md-4 mx-md-1 mb-2" href="/laptops" role="button">Geen aanbieding, maar wel te koop!</a>
                        <a class="t-btn s-lg d-bl standard col-md-4 mx-md-1 mb-2" href="#top-offers" role="button">Bekijk de Top 12 aanbiedingen</a>
                        <a class="t-btn s-lg d-none d-bl d-md-block standard col-md-4 mx-md-1 mb-2" href="#articles-home" role="button">Bekijk de nieuwste artikelen</a>
                    </p>
                </div>
            </div>
        </div>

        <div id="top-offers">

            <h2 class="text-center mb-5 px-3">{{ setting('site.top_offers_header') }}</h2>
            <top-offers></top-offers>
        </div>

        <div id="articles-home" class="blogs d-none d-md-block">
            <div id="articles-home-overlay">
                <newest-articles></newest-articles>
            </div>
        </div>

        <btt-button scroll-trigger="#hero-home"></btt-button>
    </div>
@endsection
