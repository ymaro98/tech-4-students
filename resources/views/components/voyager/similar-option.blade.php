@foreach ($options as $option)
    @if($option->selected == true)
        <option value="{{ $option->name }}" selected>{{ $option->name }}</option>
    @else
        <option value="{{ $option->name }}">{{ $option->name }}</option>
    @endif
@endforeach
