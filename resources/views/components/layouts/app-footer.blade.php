<!-- Footer -->
<footer class="page-footer font-small mdb-color pt-4">

    <!-- Footer Links -->
    <div class="container text-center text-md-left">

        <!-- Footer links -->
        <div class="row text-center text-md-left mt-3 pb-3">

            <!-- Grid column -->
            <div class="col-md-3 col-xl-4 mx-auto mt-3">
                <h6 class="text-uppercase mb-4 font-weight-bold">
                    {{ setting('site.title') }} in het kort
                </h6>
                <p>
                    {{ setting('site.footer_text') }}
                </p>
            </div>
            <!-- Grid column -->

            <hr class="w-100 clearfix d-md-none">

            <!-- Grid column -->
            <div class="col-md-3 col-xl-3 mx-auto mt-3">
                <h6 class="text-uppercase mb-4 font-weight-bold">
                    {{ setting('site.url') }}
                </h6>
                {{ menu('footer_blok_1', 'partials.menus.footer_block') }}
            </div>
            <!-- Grid column -->

            <hr class="w-100 clearfix d-md-none">

            <!-- Grid column -->
            <div class="col-md-3 col-xl-3 mx-auto mt-3">
                <h6 class="text-uppercase mb-4 font-weight-bold">
                    Handige Links
                </h6>
                {{ menu('footer_blok_2', 'partials.menus.footer_block') }}
            </div>

            <!-- Grid column -->
            <hr class="w-100 clearfix d-md-none">

            <!-- Grid column -->
            <div class="col-md-3 col-xl-2 mx-auto mt-3">
                <h6 class="text-uppercase mb-4 font-weight-bold">
                    Algemeen
                </h6>
                {{ menu('footer_blok_3', 'partials.menus.footer_block') }}
            </div>
            <!-- Grid column -->

            </div>
            <!-- Footer links -->

            <hr>

            <!-- Grid row -->
            <div class="row d-flex align-items-center">

            <!-- Grid column -->
            <div class="col-md-7 col-lg-8">

            <!--Copyright-->
            <p class="text-center text-md-left">
                © {{ \Carbon\Carbon::now()->year }} Copyright:
                <a href="/">
                    <strong class="text-dark"> {{ setting('site.title') }}</strong>
                </a>
            </p>

            {{-- <p class="text-center text-md-left">
                <a href="/">
                    <strong>{{ setting('site.url') }}</strong>
                </a>
                is onderdeel van: 
                <span class="d-inline-block">
                    <a href="https://ymaroblue.nl">
                        <strong> WB Development & Marketing</strong>
                    </a>
                </span>
            </p> --}}

            </div>
            <!-- Grid column -->

            <!-- Grid column -->
            <div class="col-md-5 col-lg-4 ml-lg-0">

            <!-- Social buttons -->
            <div class="text-center text-md-right">
                <ul class="list-unstyled list-inline">
                    <li class="list-inline-item">
                        <a class="btn-floating btn-sm rgba-white-slight mx-1">
                            <i class="fab fa-facebook-f"></i>
                        </a>
                    </li>
                    <li class="list-inline-item">
                        <a class="btn-floating btn-sm rgba-white-slight mx-1">
                            <i class="fab fa-twitter"></i>
                        </a>
                    </li>
                    <li class="list-inline-item">
                        <a class="btn-floating btn-sm rgba-white-slight mx-1">
                            <i class="fab fa-google-plus-g"></i>
                        </a>
                    </li>
                    <li class="list-inline-item">
                        <a class="btn-floating btn-sm rgba-white-slight mx-1">
                            <i class="fab fa-linkedin-in"></i>
                        </a>
                    </li>
                </ul>
            </div>

            </div>
            <!-- Grid column -->

        </div>
        <!-- Grid row -->

    </div>
    <!-- Footer Links -->

</footer>
<!-- Footer -->
