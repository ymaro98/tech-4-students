@extends('layouts.app')

@section('title', str_ireplace(["[product]", "[winkel]"], [$product->product->name, $webshop->name], $page->title))
@section('meta_description', str_ireplace(["[product]", "[winkel]"], [$product->product->name, $webshop->name], $page->meta_description))
@section('meta_keywords', str_ireplace(["[product]", "[winkel]"], [$product->product->name, $webshop->name], $page->meta_keywords))

@section('content')

    <section class="container">

        <div class="row">

            <div class="card px-4 col-xs-12 col-lg-6 mb-2 mb-lg-0 mx-3 mx-lg-0">

                <div class="p-4">
                    <div class="product-card-image-box">
                        <img class="img-fluid product-card-image" src="{{ $product->product->image}}" alt="">
                    </div>
                </div>
            </div>

            <div class="col-xs-12 col-lg-6">

                <div class="card p-4">

                    @if($product->offer && $product->offer->active === 1)
                        <percentage-ribbon vanprijs="{{$product->offer->vanprijs}}" voorprijs="{{$product->offer->voorprijs}}"></percentage-ribbon>
                    @endif

                    <h2>{{ $product->product->name }}</h2>
                    <hr>

                    <div class="prijs-info">

                        <div class="row">

                            <div id="prijzen" class="col-12">

                                @if($product->offer && $product->offer->active === 1)
                                    <h3 class="text-center">Nu in de aanbieding!!!</h3>
                                    <h2 class="voorprijs d-block text-center t-bg-yellow rounded-pill">
                                        <span class="font-weight-bold">€ {{ $product->offer->voorprijs }}</span>
                                    </h2>
                                    <h4 class="vanprijs d-inline price-old text-left text-danger rounded-pill">
                                        <del><span class="font-weight-bold d-inline">€ {{ $product->offer->vanprijs }}</span></del>
                                    </h4>
                                    <h4 class="d-inline float-right">
                                        <span class="badge badge-pill badge-success">Je bespaart € {{ $product->offer->vanprijs - $product->offer->voorprijs}}</span>
                                    </h4>
                                @else
                                    <h2 class="voorprijs d-block text-center t-bg-yellow rounded-pill">
                                        <span class="font-weight-bold">€ {{ $product->prijs }}</span>
                                    </h2>
                                @endif
                            </div>
                        </div>
                    </div>
                    <hr>

                    <div class="description">

                        <p>{{ $product->product->description }}</p>
                    </div>
                    <hr>

                    <cloak-shop-url css="btn btn-primary btn-block btn-bestel"
                    url="{{ $product->affiliate_url ?? $product->url }}" button-text="Nu bestellen op {{ $product->webshop->name }}"></cloak-shop-url>
                </div>
            </div>
        </div>
    </section>
@endsection
