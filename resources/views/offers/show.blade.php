@extends('layouts.app')

@section('title', str_ireplace(["[product]", "[winkel]"], [$offer->webshopProduct->product->name, $offer->webshopProduct->webshop->name], $page->title))
@section('meta_description', str_ireplace(["[product]", "[winkel]"], [$offer->webshopProduct->product->name, $offer->webshopProduct->webshop->name], $page->meta_description))
@section('meta_keywords', str_ireplace(["[product]", "[winkel]"], [$offer->webshopProduct->product->name, $offer->webshopProduct->webshop->name], $page->meta_keywords))

@section('content')

    <section class="container">

        <div class="row justify-content-around">

            <div class="card px-4 col-xs-12 col-lg-6 mb-2 mb-lg-0 mx-3 mx-lg-0">

                <div class="p-4">
                    <div class="product-card-image-box">
                        <img class="img-fluid product-card-image" src="{{ $offer->webshopProduct->product->image}}" alt="">
                    </div>
                </div>
            </div>

            <div class="col-xs-12 col-lg-6">

                <div class="card p-4">

                    @if($offer->active == 1)
                        <percentage-ribbon vanprijs="{{$offer->vanprijs}}" voorprijs="{{$offer->voorprijs}}"></percentage-ribbon>
                    @endif

                    <h2>{{ $offer->webshopProduct->product->name }}</h2>
                    <hr>

                    <div class="prijs-info">

                        <div class="row">

                            <div id="prijzen" class="col-12">

                                <h2 class="voorprijs d-block text-center t-bg-yellow rounded-pill">
                                    <span class="font-weight-bold">€ {{ $offer->voorprijs }}</span>
                                </h2>

                                @if($offer->active == 1)

                                    <h4 class="vanprijs d-inline price-old text-left text-danger rounded-pill">
                                        <del><span class="font-weight-bold d-inline">€ {{ $offer->vanprijs }}</span></del>
                                    </h4>
                                    <h4 class="d-inline float-right">
                                        <span class="badge badge-pill badge-success">Je bespaart € {{ $offer->vanprijs - $offer->voorprijs }}</span>
                                    </h4>
                                @endif
                            </div>
                        </div>
                    </div>
                    <hr>

                    <div class="description">

                        @if($offer->description)
                            <p>{{ $offer->description }}</p>
                        @else
                            <p>{{ $offer->webshopProduct->product->description }}</p>
                        @endif
                    </div>
                    <hr>

                    @if($offer->active == 1)
                        <cloak-shop-url css="btn btn-block btn-primary btn-bestel"
                        url="{{ $offer->webshopProduct->affiliate_url ?? $offer->webshopProduct->url }}" button-text="Nu bestellen op {{ $offer->webshopProduct->webshop->name }}"></cloak-shop-url>
                    @else
                        <cloak-shop-url css="btn btn-block btn-danger btn-bestel"
                        url="{{ $offer->webshopProduct->affiliate_url ?? $offer->webshopProduct->url }}" button-text="Niet leverbaar bij {{ $offer->webshopProduct->webshop->name }}"></cloak-shop-url>
                    @endif
                </div>
            </div>
        </div>
    </section>
@endsection
