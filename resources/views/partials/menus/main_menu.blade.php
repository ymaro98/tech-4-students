@php

    if (Voyager::translatable($items)) {
        $items = $items->load('translations');
    }

@endphp

@foreach ($items as $item)
    <li class="nav-item 
        {{ ((request()->path() != '/') ? "/".request()->path() : request()->path()) == $item->link() 
        ? 'active' 
        : ''}}">
        <a class="p-2 text-dark nav-link" href="{{ url($item->link()) }}" target="{{ $item->target }}">
            <span>{{ $item->title }}</span>
        </a>
    </li>
@endforeach

<!-- Authentication Links -->
@auth
    <li class="nav-item dropdown">
        <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
            {{ Auth::user()->name }} <span class="caret"></span>
        </a>

        <div class="dropdown-menu dropdown-menu-left" aria-labelledby="navbarDropdown">
            <a class="dropdown-item" href="/WB" target="_blank">
                Dashboard
            </a>
            <a class="dropdown-item" href="{{ route('logout') }}"
                onclick="event.preventDefault();
                                document.getElementById('logout-form').submit();">
                {{ __('Logout') }}
            </a>

            @if( config('app.url') == 'http://techsale.test')

                <div class="dropdown-divider"></div>

                <a class="dropdown-item" href="https://tech-sale.nl" target="_blank">
                    tech-sale.nl
                </a>

                <a class="dropdown-item" href="https://tech4students.nl" target="_blank">
                    tech4students.nl
                </a>
            @endif

            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
            </form>
        </div>
    </li>
@endauth
