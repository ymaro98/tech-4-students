@foreach ($items as $item)
    <p>
        <a href="{{ url($item->link()) }}" target="{{ $item->target }}">
            <span class="text-dark">{{ $item->title }}</span>
        </a>
    </p>
@endforeach