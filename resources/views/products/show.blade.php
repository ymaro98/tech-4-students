@extends('layouts.app')

@section('title', str_ireplace(["[product]"], [$product->name], $page->title))
@section('meta_description', str_ireplace(["[product]"], [$product->name], $page->meta_description))
@section('meta_keywords', str_ireplace(["[product]"], [$product->name], $page->meta_keywords))

@section('content')

    <section class="container">

        <div class="row">

            <div class="card px-4 col-xs-12 col-lg-6 mb-2 mb-lg-0 mx-3 mx-lg-0">

                <div class="p-4">
                    <div class="product-card-image-box">
                        <img class="img-fluid product-card-image" src="{{ $product->image}}" alt="">
                    </div>
                </div>
            </div>

            <div class="col-xs-12 col-lg-6">

                <div class="card p-4">

                    <h2>{{ $product->name }}</h2>
                    <hr>

                    <div class="prijs-info mx-sm-2">

                        @if($offers)
                            <table class="table">
                                <h2>In de aanbieding bij:</h2>
                                <thead class="t-bg-yellow">
                                    <tr>
                                        <th>Winkel</th>
                                        <th>Van</th>
                                        <th>Voor</th>
                                        <th class="text-center">Nu bestellen</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {{-- @foreach ($product->webshopProducts as $shopProduct)
                                        @foreach ($shopProduct->offers as $offer)
                                            <tr>
                                                <td>{{ $shopProduct->webshop->name}}</td>
                                                <td>€ {{ $offer->vanprijs }}</td>
                                                <td>€ {{ $offer->voorprijs }}</td>
                                                <td>
                                                    <a class="btn btn-sm btn-block btn-primary" href="{{ $shopProduct->url }}" role="button">bestellen</a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endforeach --}}
                                    @foreach ($offers as $offer)
                                        <tr>
                                            <td>{{ $offer->webshopProduct->webshop->name}}</td>
                                            <td>€ {{ $offer->vanprijs }}</td>
                                            <td>€ {{ $offer->voorprijs }}</td>
                                            <td>
                                                <cloak-shop-url css="btn btn-sm btn-block btn-primary"
                                                url="{{ $offer->webshopProduct->url }}" button-text="bestellen"></cloak-shop-url>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        @endif

                        @if(count($product->webshopProducts))
                            <table class="table">
                                <h2>Te koop bij:</h2>
                                <thead class="t-bg-yellow">
                                    <tr>
                                        <th>Winkel</th>
                                        <th>Normale Verkoopprijs</th>
                                        <th class="text-center">Nu bestellen</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($product->webshopProducts as $shopProduct)
                                        <tr>
                                            <td>{{ $shopProduct->webshop->name}}</td>
                                            <td>€ {{ $shopProduct->prijs }}</td>
                                            <td>
                                                <cloak-shop-url css="btn btn-sm btn-block btn-primary"
                                                url="{{ $shopProduct->url }}" button-text="bestellen"></cloak-shop-url>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        @endif
                    </div>

                    <hr>

                    @if ($product->description)

                        <div class="description">
                            <p>{{ $product->description }}</p>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </section>
@endsection
