@extends('layouts.app')

@section('title', $page->title)
@section('meta_description', $page->meta_description)
@section('meta_keywords', $page->meta_keywords)

@section('content')
<header class="masthead" style="background-image: url('{{ Voyager::image($article->thumbnail) }}')">
    <div class="overlay"></div>
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-md-10 mx-auto">
                <div class="post-heading">
                    <h1>{{ $article->title }}</h1>
                    <h2 class="subheading">{{ $article->intro }}</h2>
                    <span class="meta">Gepost door
                        <a href="#">{{ $article->user->name }}</a>
                        op {{ $article->created_at->translatedFormat('l j F Y H:i:s') }}
                    </span>
                </div>
            </div>
        </div>
    </div>
</header>

<!-- Post Content -->
<article>
    <div class="container">
        {!! $article->body !!}
    </div>
</article>
@endsection
