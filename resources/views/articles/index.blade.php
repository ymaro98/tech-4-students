@extends('layouts.app')

@section('title', $page->title)
@section('meta_description', $page->meta_description)
@section('meta_keywords', $page->meta_keywords)

@section('extra_styles')

@php
    $bg = Voyager::image($page->image);
@endphp
<style>
    #articles-top-overlay{
        position: relative;
        display: block;
        width: 100%;
        min-height: 500px;
        padding: 5rem 2rem;
        background-color: rgba(0,0,0,0.5);
        z-index: 0;
    }

    #articles-top{
        background-image: url({{ $bg }});
        /* Set a specific height */
        min-height: 500px;

        /* Create the parallax scrolling effect */
        background-attachment: fixed;
        background-position: center;
        background-repeat: no-repeat;
        background-size: cover;
    }
</style>
@endsection('extra_styles')

@section('content')

<header id="articles-top" class="masthead">
    <div id="articles-top-overlay">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-md-10 mx-auto">
                    <div class="site-heading">
                        <h1>{{ $page->title }}</h1>
                        <span class="subheading">{{ $page->intro }}</span>
                    </div>
                </div>
            </div>
            <scroll-down target="artikelen"></scroll-down>
        </div>
    </div>
</header>

<!-- Main Content -->
<div id="artikelen" class="container">
    <div class="row">
        <div class="col-lg-8 col-md-10 mx-auto">
            @foreach ($articles as $article)

                <div class="post-preview">
                    <a href="{{ route('articles.show', ["slug" => $article->title_slug]) }}">
                        <h2 class="post-title">
                            {{ $article->title }}
                        </h2>
                        <h3 class="post-subtitle">
                            {{ $article->intro }}
                        </h3>
                    </a>
                    <p class="post-meta">Gepost door
                        <a href="#">{{ $article->user->name }}</a>
                        op {{ $article->created_at->translatedFormat('l j F Y H:i:s') }}
                    </p>
                </div>
                <hr>
            @endforeach

            <!-- Pager -->
            <div class="clearfix">
                @if($articles->currentPage() != $articles->onFirstPage())
                <a class="t-btn s-md standard float-left paginate-old-new" href="{{ $articles->previousPageUrl() }}">&larr; Nieuwere Artikelen</a>
                @endif

                @if($articles->currentPage() != $articles->lastPage())
                <a class="t-btn s-md standard float-right paginate-old-new" href="{{ $articles->nextPageUrl() }}">Oudere Artikelen &rarr;</a>
                @endif
            </div>
        </div>
    </div>
</div>
@endsection
