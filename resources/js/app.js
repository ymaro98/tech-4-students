/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
require('./smoothScroll.js');

window.Vue = require('vue');

// Fontawsome icons
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';
import { library } from '@fortawesome/fontawesome-svg-core';
import { faChevronUp, faChevronDown, faFilter, faLaptop, faEdit, faArrowUp} from '@fortawesome/free-solid-svg-icons';
library.add([faChevronUp, faChevronDown, faFilter, faLaptop, faEdit, faArrowUp])

// Element-ui components
import { InputNumber, Checkbox, CheckboxGroup } from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
Vue.component(InputNumber.name, InputNumber);
Vue.component(Checkbox.name, Checkbox);
Vue.component(CheckboxGroup.name, CheckboxGroup);

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.component('edit-page', require('./components/admin/EditPage.vue').default);

Vue.component('top-offers', require('./components/offers/TopOffers.vue').default);
Vue.component('offers', require('./components/offers/Offers.vue').default);
Vue.component('products', require('./components/products/Products.vue').default);
Vue.component('webshops', require('./components/webshops/Webshops.vue').default);
Vue.component('webshop-products', require('./components/shopProducts/WebshopProducts.vue').default);

Vue.component('newest-articles', require('./components/articles/NewestArticles.vue').default);

Vue.component('shop-filter', require('./components/sidebarFilters/Shop.vue').default);
Vue.component('brand-filter', require('./components/sidebarFilters/Brand.vue').default);
Vue.component('system-filter', require('./components/sidebarFilters/System.vue').default);
Vue.component('cpuFam-filter', require('./components/sidebarFilters/CpuFam.vue').default);
Vue.component('cpuModel-filter', require('./components/sidebarFilters/CpuModel.vue').default);
Vue.component('cpuGhz-filter', require('./components/sidebarFilters/CpuGhz.vue').default);
Vue.component('gpuFam-filter', require('./components/sidebarFilters/GpuFam.vue').default);
Vue.component('gpuModel-filter', require('./components/sidebarFilters/GpuModel.vue').default);
Vue.component('ram-filter', require('./components/sidebarFilters/Ram.vue').default);

Vue.component('percentage-ribbon', require('./components/other/PercentageRibbon.vue').default);
Vue.component('cloak-shop-url', require('./components/other/CloakShopUrl.vue').default);
Vue.component('btt-button', require('./components/other/BackToTopButton.vue').default);
Vue.component('scroll-down', require('./components/other/ScrollDownButton.vue').default);
Vue.component('btf-button', require('./components/other/BackToFiltersButton.vue').default);
Vue.component('go-to-offers', require('./components/other/GoToOffersButton.vue').default);
Vue.component('font-awesome-icon', FontAwesomeIcon);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
});

