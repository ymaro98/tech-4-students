$(document).ready(function() {
    $('a[href^="#"]').click(function(e) {

        let noDropdowns = e.target.dataset.toggle;

        if(noDropdowns != 'dropdown'){

            var target = $(this.hash);
            if (target.length == 0) target = $('a[name="' + this.hash.substr(1) + '"]');
            if (target.length == 0) target = $('html');
            $('html, body').animate({ scrollTop: target.offset().top-60 }, 700);
            return false;
        }
    });
});
