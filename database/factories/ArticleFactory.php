<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Article;
use Faker\Generator as Faker;
use Carbon\Carbon;
use Illuminate\Support\Str;


$factory->define(Article::class, function (Faker $faker) {
    return [
        'title' => $faker->sentence(10),
        'title_slug' => Str::slug($faker->sentence(10)),
        'intro' => $faker->sentence(1),
        'thumbnail' => $faker->sentence(1),
        'body' => $faker->paragraphs(5, true),
        'created_at' => $faker->dateTimeBetween('-3 months'),
    ];
});

$factory->state(App\Article::class, 'test-post', function (Faker $faker){
    return [
        'title' => 'Testing Admin Article',
        'content' => 'Content of the Admin Article',
        'user_id' => 1,
        'created_at' => Carbon::now()->toDateTimeString(),
    ];
});

