<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Product;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(Product::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'name_slug' => Str::slug($faker->name, '_'),
        'description' => $faker->sentence(3),
        'image' => 'https://via.placeholder.com/300',
    ];
});
