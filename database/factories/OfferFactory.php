<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Offer;
use Faker\Generator as Faker;

$factory->define(Offer::class, function (Faker $faker) {
    return [
        'vanprijs' => $faker->numberBetween(1200, 2895),
        'voorprijs' => $faker->numberBetween(250, 1200),
        'description' => $faker->sentence(5),
        'start_date' => $faker->dateTimeBetween('-3 months'),
        'is_automatic' => $faker->numberBetween(0, 1),
        'active' => $faker->numberBetween(0, 1),
    ];
});
