<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\WebshopProduct;
use Faker\Generator as Faker;

$factory->define(WebshopProduct::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'prijs' => $faker->numberBetween(1200, 2895),
        'url' => 'https://tech-sale.nl/',
        'active' => $faker->numberBetween(0, 1),
    ];
});
