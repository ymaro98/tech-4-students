<?php

use App\Offer;
use App\WebshopProduct;
use Illuminate\Database\Seeder;

class OffersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $offerCount = (int) $this->command->ask('How many offers would you like?', 50);
        $shopProducts = WebshopProduct::all();

        factory(Offer::class, $offerCount)->make()->each(function($product) use ($shopProducts){

            $product->webshop_product_id = $shopProducts->random()->id;
            $product->save();
        });
    }
}
