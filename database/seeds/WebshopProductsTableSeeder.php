<?php

use App\Product;
use App\Webshop;
use App\WebshopProduct;
use Illuminate\Database\Seeder;

class WebshopProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $shopProductCount = (int) $this->command->ask('How many shop products would you like?', 50);
        $webshops = Webshop::all();
        $products = Product::all();

        factory(WebshopProduct::class, $shopProductCount)->make()->each(function($product) use ($webshops, $products){

            $product->webshop_id = $webshops->random()->id;
            $product->product_id = $products->random()->id;
            $product->save();
        });
    }
}
