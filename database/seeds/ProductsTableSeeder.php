<?php

use App\Brand;
use App\Product;
use App\SoortProduct;
use App\Color;
use App\CpuFamily;
use App\CpuGhz;
use App\CpuModel;
use App\OperatingSystem;
use App\Ram;
use App\Resolution;
use App\ScreenSize;
use App\StorageSize;
use App\Videocard;
use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $productCount = (int) $this->command->ask('How many products would you like?', 50);
        $brands = Brand::all();
        $prod_cats = SoortProduct::all();
        $systems = OperatingSystem::all();
        $resolutions = Resolution::all();
        $screensizes = ScreenSize::all();
        $cpu_fams = CpuFamily::all();
        $cpu_ghzs = CpuGhz::all();
        $cpu_models = CpuModel::all();
        $videocards = Videocard::all();
        $rams = Ram::all();
        $storages = StorageSize::all();
        $colors = Color::all();

        factory(Product::class, $productCount)->make()->each(function($product)
        use ($brands, $prod_cats, $systems, $resolutions, $screensizes, $cpu_fams, $cpu_ghzs, $cpu_models, $videocards, $rams, $storages, $colors){

            $product->brand_id = $brands->random()->id;
            $product->soort_product_id = $prod_cats->random()->id;
            $product->system_id = $systems->random()->id;
            $product->resolution_id = $resolutions->random()->id;
            $product->screen_size_id = $screensizes->random()->id;
            $product->cpu_fam_id = $cpu_fams->random()->id;
            $product->cpu_model_id = $cpu_models->random()->id;
            $product->cpu_ghz_id = $cpu_ghzs->random()->id;
            $product->videocard_id = $videocards->random()->id;
            $product->ram_id = $rams->random()->id;
            $product->storage_size_id = $storages->random()->id;
            $product->color_id = $colors->random()->id;
            $product->save();
        });
    }
}
