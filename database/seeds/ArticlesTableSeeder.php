<?php

use App\Article;
use App\User;
use Illuminate\Database\Seeder;

class ArticlesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $articleCount = (int) $this->command->ask('How many posts would you like?', 50);
        $users = User::all();

        factory(Article::class, $articleCount)->make()->each(function($post) use ($users){
            $post->author_id = $users->random()->id;
            $post->save();
        });
        // factory(Article::class)->states('test-post')->create();
    }
}
