<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateArticlesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('articles', function (Blueprint $table) {

            $table->bigIncrements('id');
            $table->string('title');
            $table->string('title_slug');
            $table->string('intro');
            $table->string('thumbnail');
            $table->text('body');
            $table->string('meta_keywords')->nullable();
            $table->string('meta_description')->nullable();
            $table->string('article_keywords')->nullable();
            $table->string('article_description')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('articles');
    }
}
