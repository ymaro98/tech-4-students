<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ProductController;
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

// Open Api's
Route::get('/bol-api/{cat_id}/{offers}/{offset?}/{limit?}', 'BolOpenAPI@getProducts')->name('api.bol');

// T4S Api's
Route::post('/t4s/products', 'ProductController@productsApi')->name('api.t4s.products');
Route::post('/t4s/webshop-products/{webshop_id}', 'WebshopProductController@shopProductsApi')->name('api.t4s.webshop-products');
Route::post('/t4s/offers', 'OfferController@allOffersApi')->name('api.t4s.all-offers');
Route::post('/t4s/offers/top', 'OfferController@topOffersApi')->name('api.t4s.top-offers');
Route::post('/t4s/webshops', 'ShopController@webshopsApi')->name('api.t4s.webshops');
Route::post('/t4s/articles/newest', 'ArticleController@newest')->name('api.t4s.newest-articles');

// Filter Api's
Route::get('/t4s/filters/offers', 'FilterController@fetchOfferFilters')->name('api.t4s.filters.offers');
Route::get('/t4s/filters/products', 'FilterController@fetchProductFilters')->name('api.t4s.filters.products');
Route::get('/t4s/filters/shop-products', 'FilterController@fetchShopProductFilters')->name('api.t4s.filters.shop-products');

