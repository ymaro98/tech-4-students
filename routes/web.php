<?php

use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use TCG\Voyager\Facades\Voyager;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PagesController@home')->name('home');
Route::get('/home', 'HomeController@index')->name('home');

// Offers
Route::get('/aanbiedingen', 'OfferController@index')->name('offer.index');

// Products
Route::get('/laptops', 'ProductController@index')->name('laptops');
Route::get('/laptop/{laptop}', 'ProductController@show')->name('laptop.show');

// Webshops
Route::get('/webshops', 'ShopController@index')->name('webshops');
Route::get('/webshop/{webshop}', 'ShopController@show')->name('webshop.show');

// Webshop Product/offer
Route::get('/webshop/{webshop}/product/{product}', 'WebshopProductController@show')->name('webshop.product.show');
Route::get('/webshop/{webshop}/aanbieding/{offer}', 'OfferController@show')->name('offer.show');

// Articles
Route::get('/artikelen', 'ArticleController@index')->name('articles.index');
Route::get('/artikel/{slug}', 'ArticleController@show')->name('articles.show');

Route::get('/redirect/shop', 'RedirectController@redirect')->name('redirect.shop');

if (!Request::is('WB'))
{
    Route::get('{slug}', 'PagesController@show');
}

Route::group(['prefix' => 'WB'], function () {
    Voyager::routes();

    //Scrapers
    Route::get('/scraper/dashboard', ['uses' => 'ScrapeController@scraperDashboard', 'as' => 'scraper_dashboard']);
    Route::get('/scraper/cronjob', 'ScrapeController@runScrapers')->name('scrapers.run');

    Route::get('/scraper/update-items/alternate', '\App\Http\Scrapers\alternate_scraper@run')->name('scraper.alternate');
    Route::get('/scraper/new-items/alternate', '\App\Http\Scrapers\alternate_scraper@checkNewProducts')->name('scraper.alternate');

    // Route::get('/scraper/update-items/amac', '\App\Http\Scrapers\amac_scraper@run')->name('scraper.amac');
    // Route::get('/scraper/new-items/amac', '\App\Http\Scrapers\amac_scraper@checkNewProducts')->name('scraper.amac');

    // Route::get('/scraper/update-items/amazon-de', '\App\Http\Scrapers\amazon-de_scraper@run')->name('scraper.amazon-de');
    // Route::get('/scraper/new-items/amazon-de', '\App\Http\Scrapers\amazon-de_scraper@checkNewProducts')->name('scraper.amazon-de');

    // Route::get('/scraper/update-items/amazon-nl', '\App\Http\Scrapers\amazon-nl_scraper@run')->name('scraper.amazon-nl');
    // Route::get('/scraper/new-items/amazon-nl', '\App\Http\Scrapers\amazon-nl_scraper@checkNewProducts')->name('scraper.amazon-nl');

    Route::get('/scraper/update-items/bcc', '\App\Http\Scrapers\bcc_scraper@run')->name('scraper.bcc');
    Route::get('/scraper/new-items/bcc', '\App\Http\Scrapers\bcc_scraper@checkNewProducts')->name('scraper.bcc');

    Route::get('/scraper/update-items/bol-com', '\App\Http\Scrapers\bol_com_scraper@run')->name('scraper.bol');
    Route::get('/scraper/new-items/bol-com', '\App\Http\Scrapers\bol_com_scraper@checkNewProducts')->name('scraper.bol');

    Route::get('/scraper/update-items/coolblue', '\App\Http\Scrapers\coolblue_scraper@run')->name('scraper.coolblue');
    Route::get('/scraper/new-items/coolblue', '\App\Http\Scrapers\coolblue_scraper@checkNewProducts')->name('scraper.coolblue');

    Route::get('/scraper/update-items/mediamarkt', '\App\Http\Scrapers\mediamarkt_scraper@run')->name('scraper.mediamarkt');
    Route::get('/scraper/new-items/mediamarkt', '\App\Http\Scrapers\mediamarkt_scraper@checkNewProducts')->name('scraper.mediamarkt');

    Route::get('/scraper/update-items/paradigit', '\App\Http\Scrapers\paradigit_scraper@run')->name('scraper.paradigit');
    Route::get('/scraper/new-items/paradigit', '\App\Http\Scrapers\paradigit_scraper@checkNewProducts')->name('scraper.paradigit');

    Route::get('/scraper/update-items/wehkamp', '\App\Http\Scrapers\wehkamp_scraper@run')->name('scraper.wehkamp');
    Route::get('/scraper/new-items/wehkamp', '\App\Http\Scrapers\wehkamp_scraper@checkNewProducts')->name('scraper.wehkamp');

    // Route::get('/scraper/update-items/name_streep', '\App\Http\Scrapers\name_streep_scraper@run')->name('scraper.name_streep');
    // Route::get('/scraper/new-items/name_streep', '\App\Http\Scrapers\name_streep_scraper@checkNewProducts')->name('scraper.name_streep');

    // Approve Products
    Route::get('/scraper/approve-items/{shop}', ['uses' => 'ApproveProductController@approveProducts', 'as' => 'approve.item']);
    Route::post('/scraper/approve-items/save', ['uses' => 'ApproveProductController@StoreApprovedProduct', 'as' => 'save.approved.item']);

    Route::get("/scraper/view/approve-products", "ScrapeController@approveProductsList")->name("approve.items.list");
});

Auth::routes();

