<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Offer extends Model
{
    protected $fillable = [
        'webshop_product_id',
        'description',
        'vanprijs',
        'vooprijs',
        'active',
        'start_date',
        'end_date',
    ];

    public function webshopProduct()
    {
        return $this->belongsTo('App\WebshopProduct');
    }

    public function scopeIsActive($query)
    {
        return $query->where('active', 1);
    }

    public function scopeIsOffer($query)
    {
        return $query->where('vanprijs', '>', 'voorprijs')->where('voorprijs', '>', 0);
    }

    public function getDiscountPrice($vanprijs, $voorprijs)
    {
        return round(($vanprijs - $voorprijs), 2);
    }

    public function getDiscountPercentage($vanprijs, $voorprijs)
    {
        return round(100 / ($vanprijs / ($vanprijs - $voorprijs)));
    }
}
