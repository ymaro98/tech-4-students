<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    protected $fillable = [
        'title',
        'title_slug',
        'intro',
        'thumbnail',
        'body',
        'meta_keywords',
        'meta_description',
        'blog_keywords',
        'blog_description',
        'author_id',
    ];

    public function scopeFindBySlug($query, $slug)
    {
        return $query->where('title_slug', $slug)
        ;
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'author_id');
    }
}
