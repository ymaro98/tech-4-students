<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CpuFamily extends Model
{
    protected $fillable = [
        'name',
        'name_slug',
    ];

    public function products()
    {
        return $this->hasMany('App\Product', 'gpu_fam_id');
    }
}
