<?php

namespace App\Http\Controllers;

use App\Article;
use App\Http\Resources\NewestArticlesResource;
use App\Page;
use Illuminate\Http\Request;

class ArticleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $page = Page::findBySlug('articles')->firstOrFail();
        $articles = Article::latest()->paginate();

        return view('articles.index', [
            'page' => $page,
            'articles' => $articles,
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\article  $article
     * @return \Illuminate\Http\Response
     */
    public function show(Article $article, $slug)
    {
        $page = Page::findBySlug('article')->first();
        $article = Article::findBySlug($slug)->first();

        return view('articles.show', [
            'page' => $page,
            'article' => $article,
        ]);
    }

    public function newest()
    {
        $articles = Article::latest()->limit(6)->get();
        return NewestArticlesResource::collection($articles);
    }
}
