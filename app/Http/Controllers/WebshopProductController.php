<?php

namespace App\Http\Controllers;

use App\Http\Resources\WebshopProductResource;
use App\Page;
use App\Webshop;
use App\WebshopProduct;
use Illuminate\Http\Request;

class WebshopProductController extends Controller
{
    public function show($webshop, $product)
    {
        $page = Page::findBySlug('webshop-product')->first();
        $webshop = Webshop::where('name_slug', $webshop)->isActive()->firstOrFail();
        $product = WebshopProduct::with(['product','webshop','offer'])->whereHas("product", function($q) use ($product){
            $q->where('name_slug', '=', $product);
        })->first();

        // if($product->offers){

        //     $offerModel = new OfferController;
        //     $offer = $product->product->name_slug;

        //     return $offerModel->show($webshop, $offer);
        // } else {
            // dd($product->offers);
        return view('webshopProducts.show', [
            'page' => $page,
            'product' => $product,
            'webshop' => $webshop
        ]);
        // }
    }

    public function update($item)
    {
        $product_id = $item->id;
        $product = WebshopProduct::find($product_id);
        $product->prijs = $item->scraper_voorprijs;
        $product->is_offer = $item->scraper_is_offer;
        $product->active = $item->scraper_active;
        $product->affiliate_url = $item->affiliate_url ?? NULL;
        $product->save();
    }

    public function shopProductsApi(Request $request, $webshop_id)
    {
        $products_q = WebshopProduct::isActive()->where('webshop_id', $webshop_id)->with(['offer','product','webshop']);
        $offset = (int) $request->get('offset');
        $searchFilter = $request->get('search');
        $brandFilter = $request->get('brandChecks');
        $systemFilter = $request->get('systemChecks');
        $cpuFamFilter = $request->get('cpuFamChecks');
        $cpuGhzFilter = $request->get('cpuGhzChecks');
        $ramFilter = $request->get('ramChecks');
        $minPrice = $request->get('minPrice');
        $maxPrice = $request->get('maxPrice');
        
        if ($searchFilter !== 'null') {

            // split on 1+ whitespace & ignore empty (eg. trailing space)
            $searchValues = preg_split('/\s+/', $searchFilter, -1, PREG_SPLIT_NO_EMPTY); 

            $products_q->where(function ($q) use ($searchValues){

                foreach ($searchValues as $value) {
                    $q->where('name', 'like', '%' . $value . '%');
                }
            });
        }

        if ($brandFilter !== 'null') {

            $brands = explode(',', $brandFilter);

            $products_q->whereHas("product.brand", function($q) use ($brands){
                $q->whereIn('name_slug', $brands);
            })->first();
        }

        if ($minPrice !== 'null') {

            $products_q->where('prijs', '>=', $minPrice);
        }

        if ($maxPrice !== 'null') {

            $products_q->where('prijs', '<=', $maxPrice);
        }

        if ($systemFilter !== 'null') {

            $systems = explode(',', $systemFilter);

            $products_q->whereHas("product.system", function($q) use ($systems){
                $q->whereIn('name_slug', $systems);
            })->first();
        }

        if ($cpuFamFilter !== 'null') {

            $cpuFams = explode(',', $cpuFamFilter);

            $products_q->whereHas("product.cpu_family", function($q) use ($cpuFams){
                $q->whereIn('name_slug', $cpuFams);
            })->first();
        }

        if ($cpuGhzFilter !== 'null') {

            $cpuFreqs = explode(',', $cpuGhzFilter);

            $products_q->whereHas("product.cpu_ghz", function($q) use ($cpuFreqs){
                $q->whereIn('name_slug', $cpuFreqs);
            })->first();
        }

        if ($ramFilter !== 'null') {

            $rams = explode(',', $ramFilter);

            $products_q->whereHas("product.ram", function($q) use ($rams){
                $q->whereIn('name_slug', $rams);
            })->first();
        }

        $products = $products_q->skip($offset)->take(24)->get();
        return WebshopProductResource::collection($products);
    }
}
