<?php

namespace App\Http\Controllers;

use App\Brand;
use App\Color;
use App\CpuFamily;
use App\CpuGhz;
use App\CpuModel;
use App\Offer;
use App\OperatingSystem;
use App\Ram;
use App\Resolution;
use App\ScreenSize;
use App\StorageSize;
use App\GpuFamily;
use App\GpuModel;
use App\Http\Resources\FilterResource;
use Illuminate\Http\Request;

class FilterController extends Controller
{
    public function brands()
    {
        $brands = Brand::orderBy('priority', 'DESC')->get();
        return $brands;
    }

    public function systems()
    {
        $systems = OperatingSystem::orderBy('name', 'ASC')->get();
        return $systems;
    }

    public function resolutions()
    {
        $resolutions = Resolution::orderBy('name', 'ASC')->get();
        return $resolutions;
    }

    public function screenSizes()
    {
        $sizes = ScreenSize::orderBy('name', 'ASC')->get();
        return $sizes;
    }

    public function cpuFams()
    {
        $cpuFams = CpuFamily::orderBy('name', 'ASC')->get();
        return $cpuFams;
    }

    public function cpuModels()
    {
        $cpuModels = CpuModel::orderBy('name', 'ASC')->get();
        return $cpuModels;
    }

    public function cpuFreqs()
    {
        $cpuFreqs = CpuGhz::orderBy('name', 'DESC')->get();
        return $cpuFreqs;
    }

    public function gpuFams()
    {
        $gpuFams = GpuFamily::orderBy('name', 'ASC')->get();
        return $gpuFams;
    }

    public function gpuModels()
    {
        $gpuModels = GpuModel::orderBy('name', 'ASC')->get();
        return $gpuModels;
    }

    public function storageSizes()
    {
        $sizes = StorageSize::orderBy('name', 'DESC')->get();
        return $sizes;
    }

    public function colors()
    {
        $colors = Color::orderBy('name', 'DESC')->get();
        return $colors;
    }

    public function rams()
    {
        $rams = Ram::orderBy('name', 'DESC')->get();
        return $rams;
    }

    public function offerPriceMinMax()
    {
        $min = Offer::min('voorprijs');
        $max = Offer::max('voorprijs');

        return [
            'min' => $min,
            'max' => $max,
        ];
    }

    public function fetchOfferFilters()
    {

        $webshops = ShopController::webshopsApi();
        $systems = $this->systems();
        $brands = $this->brands();
        $min_max = $this->offerPriceMinMax();
        $cpuFams = $this->cpuFams();
        $cpuModels = $this->cpuModels();
        $cpuFreqs = $this->cpuFreqs();
        $gpuFams = $this->gpuFams();
        $gpuModels = $this->gpuModels();
        $rams = $this->rams();

        return response()->json(array(
            'webshops' => $webshops,
            'systems' => $systems,
            'brands' => $brands,
            'min_max' => $min_max,
            'cpu_fams' => $cpuFams,
            'cpu_models' => $cpuModels,
            'cpu_freqs' => $cpuFreqs,
            'gpu_fams' => $gpuFams,
            'gpu_models' => $gpuModels,
            'rams' => $rams,
        ));
    }

    public function fetchProductFilters()
    {

        $webshops = ShopController::webshopsApi();
        $systems = $this->systems();
        $brands = $this->brands();
        $min_max = $this->offerPriceMinMax();
        $cpuFams = $this->cpuFams();
        $cpuModels = $this->cpuModels();
        $cpuFreqs = $this->cpuFreqs();
        $gpuFams = $this->gpuFams();
        $gpuModels = $this->gpuModels();
        $rams = $this->rams();

        return response()->json(array(
            'webshops' => $webshops,
            'systems' => $systems,
            'brands' => $brands,
            'min_max' => $min_max,
            'cpu_fams' => $cpuFams,
            'cpu_models' => $cpuModels,
            'cpu_freqs' => $cpuFreqs,
            'gpu_fams' => $gpuFams,
            'gpu_models' => $gpuModels,
            'rams' => $rams,
        ));
    }

    public function fetchShopProductFilters()
    {

        $systems = $this->systems();
        $brands = $this->brands();
        $min_max = $this->offerPriceMinMax();
        $cpuFams = $this->cpuFams();
        $cpuModels = $this->cpuModels();
        $cpuFreqs = $this->cpuFreqs();
        $gpuFams = $this->gpuFams();
        $gpuModels = $this->gpuModels();
        $rams = $this->rams();

        return response()->json(array(
            'systems' => $systems,
            'brands' => $brands,
            'min_max' => $min_max,
            'cpu_fams' => $cpuFams,
            'cpu_models' => $cpuModels,
            'cpu_freqs' => $cpuFreqs,
            'gpu_fams' => $gpuFams,
            'gpu_models' => $gpuModels,
            'rams' => $rams,
        ));
    }

    public function approveProductFormOptions()
    {
        $shopController = new ShopController;
        $webshops = $shopController->getAllWebshops();
        $brands = $this->brands();
        $systems = $this->systems();
        $resolutions = $this->resolutions();
        $screen_sizes = $this->screenSizes();
        $cpuFams = $this->cpuFams();
        $cpuModels = $this->cpuModels();
        $cpuFreqs = $this->cpuFreqs();
        $gpuFams = $this->gpuFams();
        $gpuModels = $this->gpuModels();
        $storage_sizes = $this->storageSizes();
        $colors = $this->colors();
        $rams = $this->rams();

        return (object) [
            'webshops' => $webshops,
            'systems' => $systems,
            'brands' => $brands,
            'resolutions' => $resolutions,
            'screen_sizes' => $screen_sizes,
            'cpu_fams' => $cpuFams,
            'cpu_models' => $cpuModels,
            'cpu_freqs' => $cpuFreqs,
            'gpu_fams' => $gpuFams,
            'gpu_models' => $gpuModels,
            'storage_sizes' => $storage_sizes,
            'colors' => $colors,
            'rams' => $rams,
        ];
    }
}
