<?php

namespace App\Http\Controllers;

use App\ApproveProduct;
use App\Webshop;
use Goutte\Client;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use TCG\Voyager\Facades\Voyager;


class ScrapeController extends Controller
{

    // Scraper Dashboard
    public function scraperDashboard()
    {
        $shopController = new \App\Http\Controllers\ShopController;
        $webshops = $shopController->getAllWebshops();

        return Voyager::view('voyager::scrapers.scraper-dashboard')->with("webshops", $webshops);;
    }

    public function approveProductsList()
    {
        $scrapData = new ApproveProduct();
        $data = $scrapData->all();
        return view('voyager::scrapers.view-approve-products')->with("data", $data);
    }

    public function runScrapers(){

        $scrapersDir = app_path() . "/Http/Scrapers/";
        $files = File::files($scrapersDir);

        $scrapers = [];
        foreach ($files as $scraper) {

            $fileName = $scraper->getFilename();
            $shop_slug = str_replace('_scraper.php', '', $fileName);
            if($shop_slug != 'template'){
                $shopActive = Webshop::findShopBySlug($shop_slug)->pluck('active')->first();
                if($shopActive){
                    $scrapers[] = $shop_slug;
                }
            }
        }

        $app = '\\App\\Http\\Scrapers\\';
        foreach ($scrapers as $scraper) {

            $scraperFile = str_replace('-', '_', $scraper) . '_scraper';
            $scraperDir = $app . $scraperFile;

            $scraper = new $scraperDir;
            $runScraper = $scraper->run();
        }
    }

    public function showResults($scraperResults)
    {
        return Voyager::view('voyager::scrapers/scraper-results', [
            'scraperResults' => $scraperResults,
        ]);
    }

    public function getProductsByShopSlug($slug)
    {
        $shop = Webshop::findShopBySlug($slug)->first();
        $products = $shop->webshopProducts;

        return $products;
    }

    public function stripChars($string, $extra = '')
    {   
        
        if($extra == 'thousand' && $string < 10){
            $newString = str_replace(
                [
                    " ",
                    ",",
                    ".-",
                    ".",
                    "-",
                    ",-",
                    "€",
                    "�",
                ],
                [
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                ],
                $string
            );
        } else {
            $newString = str_replace(
                [
                    " ",
                    ",",
                    ".-",
                    ",-",
                    "€",
                ],
                [
                    "",
                    ".",
                    "",
                    "",
                    "",
                ],
                $string
            );
        }
        return trim($newString);
    }

    public function scrapeStore($url)
    {
        $client = new Client();
        $html = $client->request('GET', $url);
        return $html;
    }

    public function checkOfferStatus($item)
    {
        $vanprijs = $item['scraper_vanprijs'];
        $voorprijs = $item['scraper_voorprijs'];

        if($vanprijs != 0 && $vanprijs > $voorprijs){
            return true;
        } else {
            return false;
        }
    }

    public function checkForOffer($item)
    {
        $offer = $item->offers->where('is_automatic', 1)->first();
        return $offer;
    }

    public function updateShopProduct($item)
    {
        $product = new WebshopProductController();
        $product->update($item);
    }

    public function updateOffer($offer, $item)
    {
        $product = new OfferController();
        $product->update($offer, $item);
    }

    public function insertNewOffer($item)
    {
        $product = new OfferController();
        $product->store($item);
    }

    public function insert($item)
    {
        $offerStatus = $this->checkOfferStatus($item);
        if(!$offerStatus){
            $isOffer = $this->checkForOffer($item);
            $this->updateShopProduct($item);
            $isOffer = $this->checkForOffer($item);
            if($isOffer){
                $this->updateOffer($isOffer, $item);
            }
        } else {
            $isOffer = $this->checkForOffer($item);
            if(!$isOffer){
                $this->insertNewOffer($item);
            } else {
                $this->updateOffer($isOffer, $item);
            }
            $this->updateShopProduct($item);
        }
    }

    public function setResultStatus($item)
    {
        $page = 'product';
        if($item['scraper_active'] == 0){

            $type = 'Niet leverbaar';
            $kleur = 'red';
        } else {

            if(($item['scraper_vanprijs'] == 0 && $item['scraper_voorprijs'] > 0) OR ($item['scraper_vanprijs'] == $item['scraper_voorprijs']) ){

                $type = 'Normale prijs';
                $kleur = 'yellow';
            } else if($item['scraper_vanprijs'] > $item['scraper_voorprijs'] && $item['scraper_voorprijs'] > 0){

                $type = 'Aanbieding';
                $page = 'aanbieding';
                $kleur = 'green';
            } else {

                $type = 'Status onbekend';
                $kleur = 'red';
            }
        }

        $status = [
            "status_type" => $type,
            "status_kleur" => $kleur,
            "page" => $page,
        ];
        return $status;
    }

    public function setInactive($item)
    {
        $item['vanprijs'] = 0;
        $item['voorprijs'] = 0;
        $item['active'] = 0;
        $item['is_offer'] = 0;

        return $item;
    }

        /**
     * Store a newly created resource in storage.
     *
     * @param  Array data
     * @return \Illuminate\Http\Response
     */
    public function saveApproveItem($data)
    {
        $scrapData = new ApproveProduct();
        $url = $data["url"];
        $data['approved'] = 0;

        if(isset($data['cpu_model'])){

            if (stripos($data['cpu_model'], 'intel') !== false) {
                $data['cpu_fam'] = 'Intel';
            } else if(stripos($data['cpu_model'], 'amd') !== false){
                $data['cpu_fam'] = 'AMD';
            }
        } else {
            $data['cpu_fam'] = 'n/a';
            $data['cpu_model'] = 'n/a';
        }

        if(isset($data['gpu_model'])){

            if (stripos($data['gpu_model'], 'intel') !== false) {
                $data['gpu_fam'] = 'Intel';
            } else if(stripos($data['gpu_model'], 'amd') !== false){
                $data['gpu_fam'] = 'AMD';
            } else if(stripos($data['gpu_model'], 'NVIDIA') !== false){
                $data['gpu_fam'] = 'NVIDIA';
            }
        } else {
            $data['gpu_fam'] = 'n/a';
            $data['gpu_model'] = 'n/a';
        }

        $scrapToUpdate = $scrapData->firstOrCreate(["url" => $url], $data);
        return $scrapToUpdate;
    }

    /**
     * Get the next specified  node after the current
     */
    public function getNextFiltered($start, string $selector)
    {
        $count = $start->parents()->count();
        $next  = $start->nextAll();
        while ($count-- > 0) {
            $filtered = $next->filter($selector);
            if ($filtered->count()) return $filtered;
            // $next = $next->nextAll();
        }

        // throw new \InvalidArgumentException('No node found');
    }

    /**
     * Create image and save it from link
     *
     */
    public function createImage($imageLink, $productName)
    {
        $image_content = file_get_contents($imageLink);

        $image_name = substr($imageLink, strrpos($imageLink, '/') + 1);
        $image_name = preg_replace("/[^a-zA-Z0-9(.)\']/", "_", $image_name);
        $ext = pathinfo($image_name, PATHINFO_EXTENSION);
        if (empty($ext)) {
            $image_name = $image_name . ".png";
        }
        $file_name = preg_replace("/[^a-zA-Z0-9\']/", "_", $productName) . "_" . $image_name;

        Storage::put("public\products\\" . $file_name, $image_content);

        return "public\products\\" . $file_name;
    }
}
