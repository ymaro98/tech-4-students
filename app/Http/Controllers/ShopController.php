<?php

namespace App\Http\Controllers;

use App\Http\Resources\WebshopResource;
use App\Page;
use App\Webshop;
use Illuminate\Http\Request;

class ShopController extends Controller
{
    public function index()
    {
        $page = Page::findBySlug('webshops')->firstOrFail();
        $webshop_count = Webshop::all()->count();

        return view('shops.index', [
            'page' => $page,
            'webshop_count' => $webshop_count,
        ]);
    }

    public function getAllWebshops()
    {
        $webshops = Webshop::orderBy('name')->get();
        return $webshops;
    }

    public function show($webshop)
    {
        $page = Page::findBySlug('webshop')->first();
        $webshop = Webshop::isActive()->where('name_slug', $webshop)->firstOrFail();

        return view('shops.show', [
            'page' => $page,
            'webshop' => $webshop,
        ]);
    }

    public static function webshopsApi()
    {
        $webshops = Webshop::isActive()
            ->withProductsCount()
            ->withOffersCount()
            ->get()
        ;

        return WebshopResource::collection($webshops);
    }
}
