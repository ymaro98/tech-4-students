<?php

namespace App\Http\Controllers;

use App\Page;
use Illuminate\Http\Request;

class PagesController extends Controller
{

    public function home()
    {
        $page = Page::findBySlug('home')->firstOrFail();
        return view('home', ['page' => $page]);
    }

    public function show($slug)
    {
        $page = Page::findBySlug($slug)->firstOrFail();
        if($page){
            $view = view('algemeen', ['page' => $page]);
        } else {
            $view = abort(404);
        }
        return $view;
    }
}
