<?php

namespace App\Http\Controllers;

use BolCom\Request AS BolRequest;
use Illuminate\Http\Request;

class BolOpenAPI extends Controller
{
    public $client;

    public function __construct()
    {
        $apiKey = config('bol-com.apiKey');
        $this->client = new BolRequest($apiKey, 'json', 0);

        return $this->client;
    }

    public function getProductsList($catIdParam, $offersParam, $offsetParam = 0, $limitParam = 100, $attributesParam = true)
    {
        $api = $this->client;

        $cat_id = "?ids=$catIdParam";
        $offers = "&offers=$offersParam";
        $offset = "&offset=$offsetParam";
        $limit = "&limit=$limitParam";
        $attributes = "&includeattributes=$attributesParam";

        $params = "$cat_id$offers$offset$limit$attributes";

        $response = $api->fetch(
            'GET',
            'https://api.bol.com/catalog/v4/lists/',
            $params
        );

        if(!$response){
            dd('Error: Geen response');
        } else {
            $products = $response->products ?? [];
        }

        return $products;
    }

    public function getProduct($url){

        $api = $this->client;

        $exp = '/\b[0-9]{16}\b/m';
        preg_match($exp, $url, $url_code);

        if(!empty($url_code)){
            $product_id = $url_code[0];

            $response = $api->fetch(
                'GET',
                "https://api.bol.com/catalog/v4/products/$product_id"
            );

            if(!$response){
                dd('error');
            } else {
                $product = $response->products[0];
            }
            return $product;
        } else {
            return false;
        }
    }
}
