<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class RedirectController extends Controller
{

    public function redirect()
    {
        $url = $_GET['redirect'] ?? '';
        if($url !== ''){
            header('HTTP/1.1 301 Moved Permanently');
            header('Location: ' . $url);
            exit();
        } else {
            return abort(404);
        }
    }
}
