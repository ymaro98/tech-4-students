<?php

namespace App\Http\Controllers;

use App\Http\Resources\OfferResource;
use App\Http\Resources\TopOffersResource;
use App\Offer;
use App\Page;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;

class OfferController extends Controller
{
    public function store($item)
    {
        $offer = new Offer();
        $offer->webshop_product_id = $item->id;
        $offer->vanprijs = $item->scraper_vanprijs;
        $offer->voorprijs = $item->scraper_voorprijs;
        $today = Carbon::today()->toDateString();
        $offer->start_date = $today;
        $offer->save();
    }

    public function update($oldOffer, $item)
    {
        $offer_id = $oldOffer->id;
        $offer = Offer::find($offer_id);
        $offer->vanprijs = $item->scraper_vanprijs;
        $offer->voorprijs = $item->scraper_voorprijs;
        $offer->active = $item->scraper_is_offer;
        $offer->save();
    }

    public function index()
    {
        $page = Page::findBySlug('aanbiedingen')->firstOrFail();
        $offers_count = Offer::isActive()->count();

        return view('offers.index', [
            'page' => $page,
            'offers_count' => $offers_count,
        ]);
    }

    public function show($webshop, $offer)
    {
        $page = Page::findBySlug('aanbieding')->firstOrFail();
        $offer = Offer::with('webshopProduct.product')->whereHas('webshopProduct.product', function ($query) use ($offer){
            $query->where('name_slug', $offer);
        })
        ->firstOrFail()
        ;

        return view('offers.show', [
            'page' => $page,
            'offer' => $offer
        ]);
    }

    public function topOffersApi()
    {
        $offers = Offer::select(
            DB::raw('

                offers.vanprijs,
                offers.voorprijs,
                offers.start_date,

                products.name as productName,
                products.name_slug as productNameSlug,
                products.image as productImage,

                webshops.name as webshopName,
                webshops.name_slug as webshopNameSlug,
                webshops.image as webshopImage,

                (100/(vanprijs/(vanprijs - voorprijs))) as kortings_percentage
                ')
            )
            ->join('webshop_products', 'offers.webshop_product_id', '=', 'webshop_products.id')
            ->join('products', 'webshop_products.product_id', '=', 'products.id')
            ->join('webshops', 'webshop_products.webshop_id', '=', 'webshops.id')
            ->where('webshops.active', 1)
            ->where('offers.active', 1)
            ->orderByDesc(DB::raw('100 /(vanprijs / (vanprijs - voorprijs))', 'desc'))
            ->limit(12)
            ->get()
        ;
        
        return TopOffersResource::collection($offers);
    }

    public function allOffersApi(Request $request)
    {
        $offers_q = Offer::isActive()->isOffer()->with(['webshopProduct.webshop', 'webshopProduct.product']);
        $offset = (int)$request->get('offset');
        $searchFilter = $request->get('search');
        $webshopFilter = $request->get('webshopChecks');
        $brandFilter = $request->get('brandChecks');
        $systemFilter = $request->get('systemChecks');
        $cpuFamFilter = $request->get('cpuFamChecks');
        $cpuModelFilter = $request->get('cpuModelChecks');
        $cpuGhzFilter = $request->get('cpuGhzChecks');
        $gpuFamFilter = $request->get('gpuFamChecks');
        $gpuModelFilter = $request->get('gpuModelChecks');
        $ramFilter = $request->get('ramChecks');
        $minPrice = $request->get('minPrice');
        $maxPrice = $request->get('maxPrice');

        if ($searchFilter !== 'null') {

            // split on 1+ whitespace & ignore empty (eg. trailing space)
            $searchValues = preg_split('/\s+/', $searchFilter, -1, PREG_SPLIT_NO_EMPTY); 

            $offers_q->whereHas("webshopProduct.product", function($q) use ($searchValues){

                foreach ($searchValues as $value) {
                    $q->where('name', 'like', "%". $value. "%");
                }
            })->first();
        }
        
        if ($webshopFilter !== 'null') {

            $webshops = explode(',', $webshopFilter);
            $offers_q->whereHas("webshopProduct.webshop", function($q) use ($webshops){
                $q->whereIn('name_slug', $webshops);
            })->first();
        }

        if ($brandFilter !== 'null') {

            $brands = explode(',', $brandFilter);

            $offers_q->whereHas("webshopProduct.product.brand", function($q) use ($brands){
                $q->whereIn('name_slug', $brands);
            })->first();
        }

        if ($minPrice !== 'null') {
            $offers_q->where('voorprijs', '>=', $minPrice);
        }

        if ($maxPrice !== 'null') {
            $offers_q->where('voorprijs', '<=', $maxPrice);
        }

        if ($systemFilter !== 'null') {

            $systems = explode(',', $systemFilter);

            $offers_q->whereHas("webshopProduct.product.system", function($q) use ($systems){
                $q->whereIn('name_slug', $systems);
            })->first();
        }

        if ($cpuFamFilter !== 'null') {

            $cpuFams = explode(',', $cpuFamFilter);

            $offers_q->whereHas("webshopProduct.product.cpu_family", function($q) use ($cpuFams){
                $q->whereIn('name_slug', $cpuFams);
            })->first();
        }

        if ($cpuModelFilter !== 'null') {

            $cpuModels = explode(',', $cpuModelFilter);

            $offers_q->whereHas("webshopProduct.product.cpu_model", function($q) use ($cpuModels){
                $q->whereIn('name_slug', $cpuModels);
            })->first();
        }

        if ($cpuGhzFilter !== 'null') {

            $cpuFreqs = explode(',', $cpuGhzFilter);

            $offers_q->whereHas("webshopProduct.product.cpu_ghz", function($q) use ($cpuFreqs){
                $q->whereIn('name_slug', $cpuFreqs);
            })->first();
        }

        if ($gpuFamFilter !== 'null') {

            $gpuFams = explode(',', $gpuFamFilter);

            $offers_q->whereHas("webshopProduct.product.gpu_family", function($q) use ($gpuFams){
                $q->whereIn('name_slug', $gpuFams);
            })->first();
        }

        if ($gpuModelFilter !== 'null') {

            $gpuModels = explode(',', $gpuModelFilter);

            $offers_q->whereHas("webshopProduct.product.gpu_model", function($q) use ($gpuModels){
                $q->whereIn('name_slug', $gpuModels);
            })->first();
        }

        if ($ramFilter !== 'null') {

            $rams = explode(',', $ramFilter);

            $offers_q->whereHas("webshopProduct.product.ram", function($q) use ($rams){
                $q->whereIn('name_slug', $rams);
            })->first();
        }

        $offers = $offers_q->skip($offset)->take(24)->get();
        return OfferResource::collection($offers);
    }
}
