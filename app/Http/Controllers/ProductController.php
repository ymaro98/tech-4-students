<?php

namespace App\Http\Controllers;

use App\Http\Resources\ProductResource;
use App\Page;
use App\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{

    public function index()
    {
        $page = Page::findBySlug('products')->firstOrFail();
        $products_count = Product::all()->count();

        return view('products.index', [
            'page' => $page,
            'products_count' => $products_count,
        ]);
    }

    public function show($product)
    {
        $page = Page::findBySlug('product')->first();
        $product = Product::where('name_slug', $product)->with(['webshopProducts.offers','webshopProducts' => function ($q) {
            $q->isActive()->checkHasOffer()->orderBy('prijs','ASC');
        }])->firstOrFail();

        $shop_products = $product->webshopProducts()->with('offers')->get();
        foreach($shop_products as $prod){

            $offersNew = [];
            $offers = $prod->offers()->isActive()->with(['webshopProduct','webshopProduct.webshop'])->get();
            if(count($offers) > 0){

                foreach($offers as $offer){
                    $offersNew[] = $offer;
                }
            }
        }

        return view('products.show', [
            'page' => $page,
            'product' => $product,
            'offers' => $offersNew,
        ]);
    }

    public function productsApi(Request $request)
    {
        $products_q = Product::with(['webshopProducts.webshop']);
        $offset = (int)$request->get('offset');
        $searchFilter = $request->get('search');
        $webshopFilter = $request->get('webshopChecks');
        $brandFilter = $request->get('brandChecks');
        $systemFilter = $request->get('systemChecks');
        $cpuFamFilter = $request->get('cpuFamChecks');
        $cpuGhzFilter = $request->get('cpuGhzChecks');
        $ramFilter = $request->get('ramChecks');
        $minPrice = $request->get('minPrice');
        $maxPrice = $request->get('maxPrice');

        if ($searchFilter !== 'null') {

            // split on 1+ whitespace & ignore empty (eg. trailing space)
            $searchValues = preg_split('/\s+/', $searchFilter, -1, PREG_SPLIT_NO_EMPTY); 

            $products_q->where(function ($q) use ($searchValues){

                foreach ($searchValues as $value) {
                    $q->where('name', 'like', '%' . $value . '%');
                }
            });
        }

        if ($webshopFilter !== 'null') {

            $webshops = explode(',', $webshopFilter);

            $products_q->whereHas("webshopProducts.webshop", function($q) use ($webshops){
                $q->whereIn('name_slug', $webshops);
            })->first();
        }

        if ($brandFilter !== 'null') {

            $brands = explode(',', $brandFilter);

            $products_q->whereHas("brand", function($q) use ($brands){
                $q->whereIn('name_slug', $brands);
            })->first();
        }

        if ($minPrice !== 'null') {

            $products_q->whereHas("webshopProducts", function($q) use ($minPrice){

                $q->where('prijs', '>=', $minPrice);
            })->first();
        }

        if ($maxPrice !== 'null') {

            $products_q->whereHas("webshopProducts", function($q) use ($maxPrice){

                $q->where('prijs', '<=', $maxPrice);
            })->first();
        }

        if ($systemFilter !== 'null') {

            $systems = explode(',', $systemFilter);

            $products_q->whereHas("system", function($q) use ($systems){
                $q->whereIn('name_slug', $systems);
            })->first();
        }

        if ($cpuFamFilter !== 'null') {

            $cpuFams = explode(',', $cpuFamFilter);

            $products_q->whereHas("cpu_family", function($q) use ($cpuFams){
                $q->whereIn('name_slug', $cpuFams);
            })->first();
        }

        if ($cpuGhzFilter !== 'null') {

            $cpuFreqs = explode(',', $cpuGhzFilter);

            $products_q->whereHas("cpu_ghz", function($q) use ($cpuFreqs){
                $q->whereIn('name_slug', $cpuFreqs);
            })->first();
        }

        if ($ramFilter !== 'null') {

            $rams = explode(',', $ramFilter);

            $products_q->whereHas("ram", function($q) use ($rams){
                $q->whereIn('name_slug', $rams);
            })->first();
        }


        $products = $products_q->skip($offset)->take(24)->get();
        return ProductResource::collection($products);
    }
}
