<?php

namespace App\Http\Controllers;

use App\ApproveProduct;
use App\Brand;
use App\Color;
use App\CpuFamily;
use App\CpuGhz;
use App\CpuModel;
use App\GpuFamily;
use App\GpuModel;
use App\OperatingSystem;
use App\Product;
use App\Ram;
use App\Resolution;
use App\ScreenSize;
use App\StorageSize;
use App\Videocard;
use App\Webshop;
use App\WebshopProduct;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use TCG\Voyager\Facades\Voyager;


class ApproveProductController extends Controller
{
    public function approveProducts($shop)
    {

        $temp_product = ApproveProduct::isNotApproved()->orderBy('created_at', 'ASC');
        if($shop != 'all'){
            $temp_product->whereShop($shop);
        }
        $productsToApprove = $temp_product->count();
        $temp_product = $temp_product->limit(1)->get();

        if($productsToApprove > 0){

            $temp_product = (object) $temp_product[0];
            $filter = new FilterController;
            $options = $filter->approveProductFormOptions();

            $collection = collect($options);
            $newOptions = $collection->map(function ($options, $sort) use ($temp_product){

                $exactMatch = false;
                $options = $options->map(function ($option) use ($temp_product, $sort){

                    $sort = Str::singular($sort);
                    $sort = str_replace(['cpu_freq'],['cpu_ghz'], $sort);

                    if(trim(strtolower($temp_product->$sort)) == trim(strtolower($option->name))){

                        $exactMatch = true;
                        $option->exactMatch = 1;
                        $option->temp_similarity = 1000;
                    } else {
                        $option->temp_similarity = similar_text(strtolower($temp_product->$sort), strtolower($option->name));
                    }
                    

                    return $option;
                });
                
                $highestSimilarity = (object) $options->where('temp_similarity', $options->max('temp_similarity'))->first(); // ['name' => 'test', 'price' => 600]
                if($exactMatch){
                    $highestSimilarity->where('exact_match', 1);
                }
                $highestSimilarity->selected = true;

                return $options;
            });
            $options = (object) $newOptions->all();
        } else {

            $temp_product = null;
            $options = null;
        }

        return Voyager::view('voyager::scrapers/approve-products', [
            'temp_product' => $temp_product,
            'options' => $options,
            'productsToApprove' => $productsToApprove,
        ]);
    }

    public function StoreApprovedProduct(Request $request){

        $systemName = $request->get('systems_rep') ?? $request->get('systems');
        $system = new OperatingSystem;
        $checkSystem = $system::where('name', $systemName)->first();
        if($checkSystem !== NULL){

            $system_id = $checkSystem->id;
        } else {

            $system->name = $systemName;
            $system->name_slug = Str::slug($system->name, "-");
            $system->save();
            $system_id = $system->id;
        }

        $cpuFamName = $request->get('cpu_fams_rep') ?? $request->get('cpu_fams');
        $cpuFam = new CpuFamily;
        $checkCpuFam = $cpuFam::where('name', $cpuFamName)->first();
        if($checkCpuFam !== NULL){

            $cpuFam_id = $checkCpuFam->id;
        } else {

            $cpuFam->name = $cpuFamName;
            $cpuFam->name_slug = Str::slug($cpuFam->name, "-");
            $cpuFam->save();
            $cpuFam_id = $cpuFam->id;
        }

        $cpuModelName = $request->get('cpu_models_rep') ?? $request->get('cpu_models');
        $cpuModel = new CpuModel;
        $checkCpuModel = $cpuModel::where('name', $cpuModelName)->first();
        if($checkCpuModel !== NULL){

            $cpuModel_id = $checkCpuModel->id;
        } else {

            $cpuModel->name = $cpuModelName;
            $cpuModel->name_slug = Str::slug($cpuModel->name, "-");
            $cpuModel->save();
            $cpuModel_id = $cpuModel->id;
        }

        $cpuFreqName = $request->get('cpu_freqs_rep') ?? $request->get('cpu_freqs');
        $cpuFreq = new CpuGhz;
        $checkCpuFreq = $cpuFreq::where('name', $cpuFreqName)->first();
        if($checkCpuFreq !== NULL){

            $cpuFreq_id = $checkCpuFreq->id;
        } else {

            $cpuFreq->name = $cpuFreqName;
            $cpuFreq->name_slug = Str::slug($cpuFreq->name, "-");
            $cpuFreq->save();
            $cpuFreq_id = $cpuFreq->id;
        }

        $resolutionName = $request->get('resolutions_rep') ?? $request->get('resolutions');
        $resolution = new Resolution;
        $checkResolution = $resolution::where('name', $resolutionName)->first();
        if($checkResolution !== NULL){
            $resolution_id = $checkResolution->id;
        } else {
            $resolution->name = $resolutionName;
            $resolution->name_slug = Str::slug($resolution->name, "-");
            $resolution->save();
            $resolution_id = $resolution->id;
        }

        $screenSizeName = $request->get('screen_sizes_rep') ?? $request->get('screen_sizes');
        $screenSize = new ScreenSize;
        $checkScreenSize = $screenSize::where('name', $screenSizeName)->first();
        if($checkScreenSize !== NULL){
            $screenSize_id = $checkScreenSize->id;
        } else {
            $screenSize->name = $screenSizeName;
            $screenSize->name_slug = Str::slug($screenSize->name, "-");
            $screenSize->save();
            $screenSize_id = $screenSize->id;
        }

        $gpuFamName = $request->get('gpu_fams_rep') ?? $request->get('gpu_fams');
        $gpuFam = new GpuFamily;
        $checkGpuFam = $gpuFam::where('name', $gpuFamName)->first();
        if($checkGpuFam !== NULL){

            $gpuFam_id = $checkGpuFam->id;
        } else {

            $gpuFam->name = $gpuFamName;
            $gpuFam->name_slug = Str::slug($gpuFam->name, "-");
            $gpuFam->save();
            $gpuFam_id = $gpuFam->id;
        }

        $gpuModelName = $request->get('gpu_models_rep') ?? $request->get('gpu_models');
        $gpuModel = new GpuModel;
        $checkGpuModel = $gpuModel::where('name', $gpuModelName)->first();
        if($checkGpuModel !== NULL){

            $gpuModel_id = $checkGpuModel->id;
        } else {

            $gpuModel->name = $gpuModelName;
            $gpuModel->name_slug = Str::slug($gpuModel->name, "-");
            $gpuModel->save();
            $gpuModel_id = $gpuModel->id;
        }

        $ramName = $request->get('rams_rep') ?? $request->get('rams');
        $ram = new Ram;
        $checkRam = $ram::where('name', $ramName)->first();

        if($checkRam !== NULL){
            $ram_id = $checkRam->id;
        } else {
            $ram->name = $ramName;
            $ram->name_slug = Str::slug($ram->name, "-");
            $ram->save();
            $ram_id = $ram->id;
        }

        $storageName = $request->get('storage_sizes_rep') ?? $request->get('storage_sizes');
        $storage = new StorageSize;
        $checkStorage = $storage::where('name', $storageName)->first();
        if($checkStorage !== NULL){
            $storage_id = $checkStorage->id;
        } else {
            $storage->name = $storageName;
            $storage->name_slug = Str::slug($storage->name, "-");
            $storage->save();
            $storage_id = $storage->id;
        }

        $colorName = $request->get('colors_rep') ?? $request->get('colors');
        $color = new Color;
        $checkColor = $color::where('name', $colorName)->first();
        if($checkColor !== NULL){

            $color_id = $checkColor->id;
        } else {

            $color->name = $colorName;
            $color->name_slug = Str::slug($color->name, "-");
            $color->save();
            $color_id = $color->id;
        }

        $brandName = $request->get('brands_rep') ?? $request->get('brands');
        $brand = new Brand;
        $checkBrand = $brand::where('name', $brandName)->first();
        if($checkBrand !== NULL){

            $brand_id = $checkBrand->id;
        } else {

            $brand->name = $brandName;
            $brand->name_slug = Str::slug($brand->name, "-");
            $brand->save();
            $brand_id = $brand->id;
        }

        $productName = $request->get('name');
        $productNameOriginal = $request->get('name_original');
        $productLink = $request->get('url');
        $image = $request->get('image');

        $updateTemp = new ApproveProduct;
        $updateTemp = $updateTemp::where('name', $productNameOriginal)->first();
        $updateTemp->approved = true;
        $updateTemp->save();

        $productModel = new Product();
        $checkProduct = $productModel::where('name', $productName)->first();
        if($checkProduct !== null){

            $product_id = $checkProduct->id;
        } else {

            $productModel->name = $productName;
            $productModel->name_slug = Str::slug($productName, "-");
            $productModel->soort_product_id = 1;
            $productModel->brand_id = $brand_id;
            $productModel->system_id = $system_id;
            $productModel->cpu_fam_id = $cpuFam_id;
            $productModel->cpu_model_id = $cpuModel_id;
            $productModel->cpu_ghz_id = $cpuFreq_id;
            $productModel->resolution_id = $resolution_id;
            $productModel->screen_size_id = $screenSize_id;
            $productModel->gpu_fam_id = $gpuFam_id;
            $productModel->gpu_model_id = $gpuModel_id;
            $productModel->ram_id = $ram_id;
            $productModel->storage_size_id = $storage_id;
            $productModel->color_id = $color_id;
            $productModel->description = "new Product";
            $productModel->image = $image;
            $productModel->save();
            $product_id = $productModel->id;
        }

        $webshopName = strtolower($request->get('webshops_rep') ?? $request->get('webshops'));
        $webshop = new Webshop;
        $checkWebshop = $webshop::where('name', $webshopName)->first();
        if($checkWebshop !== NULL){

            $webshop_id = $checkWebshop->id;
        } else {

            $webshop->name = ucfirst($webshopName);
            $webshop->name_slug = Str::slug($webshop->name, "-");
            $webshop->save();
            $webshop_id = $webshop->id;
        }

        $shopProduct = new WebshopProduct();
        $checkShopProduct = $shopProduct::where('name', $productName)->first();
        if($checkShopProduct === null){

            $shopProduct->webshop_id = $webshop_id;
            $shopProduct->product_id = $product_id;
            $shopProduct->name = $productName;
            $shopProduct->prijs = 0;
            $shopProduct->url = $productLink;
            $shopProduct->active = 0;
            $shopProduct->save();
        }

        return back();
    }
}
