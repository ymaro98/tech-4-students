<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class OfferResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // return parent::toArray($request);
        $offers = [
            'productName' => $this->webshopProduct->product->name,
            'productNameSlug' => $this->webshopProduct->product->name_slug,
            'productImage' => $this->webshopProduct->product->image,
            'description' => $this->description,
            'vanprijs' => $this->vanprijs,
            'voorprijs' => $this->voorprijs,
            'korting' => $this->getDiscountPrice($this->vanprijs, $this->voorprijs),
            'kortings_percentage' => $this->getDiscountPercentage($this->vanprijs, $this->voorprijs),
            'webshopName' => $this->webshopProduct->webshop->name,
            'webshopNameSlug' => $this->webshopProduct->webshop->name_slug,
            'webshopImage' => $this->webshopProduct->webshop->image,
        ];

        return $offers;
    }
}
