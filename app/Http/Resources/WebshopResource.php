<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class WebshopResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // return parent::toArray($request);
        return [
            'name' => $this->name,
            'name_slug' => $this->name_slug,
            'description' => $this->description,
            'image' => $this->image,
            'products_count' => $this->products_count,
            'offers_count' => $this->offers_count,
        ];
    }
}
