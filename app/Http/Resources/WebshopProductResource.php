<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class WebshopProductResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // return parent::toArray($request);
        return [
            'webshop' => $this->webshop->name,
            'webshop_slug' => $this->webshop->name_slug,
            'name' => $this->product->name,
            'name_slug' => $this->product->name_slug,
            'prijs' => $this->prijs,
            'url' => $this->url,
            'image' => $this->product->image,
            'active' => $this->active,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
}
