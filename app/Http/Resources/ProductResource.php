<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ProductResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // return parent::toArray($request);
        return [
            'name' => $this->name,
            'name_slug' => $this->name_slug,
            'description' => $this->description,
            'image' => $this->image,
            'webshop_slug' => $this->webshopProducts[0]->webshop->name_slug,
            'min_price' => $this->getMinPrice(),
            'max_price' => $this->getMaxPrice(),
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
}
