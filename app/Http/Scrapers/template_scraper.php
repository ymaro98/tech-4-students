<?php

namespace App\Http\Scrapers;

use App\Http\Controllers\ScrapeController;
use Goutte\Client;
use Illuminate\Support\Str;

class name_streep_scraper extends ScrapeController
{
    protected $shopName;
    protected $pageNumber;

    public function __construct()
    {
        $this->shopName = 'name_streep';
        $this->pageNumber = 1;
    }

    public function run()
    {
        $name = '';
        $products = $this->getProductsByShopSlug($name);

        $scraperResults = [];
        foreach ($products as $item) {

            $url = $item->url;
            $html = $this->scrapeStore($url);
            $item_div = $html->find('div[class=""]', 0);
            if(!$item_div){

                $item = $this->setInactive($item);
                $active = $item['active'];
                $is_offer = $item['is_offer'];
                $vanprijs = $item['vanprijs'];
                $voorprijs = $item['voorprijs'];
            } else {

                $voorprijs = $item_div->find('span[class=""]', 0)->plaintext;
                $voorprijs = $this->stripChars($voorprijs);
                if(!$voorprijs){

                    $item = $this->setInactive($item);
                    $active = $item['active'];
                    $is_offer = $item['is_offer'];
                    $vanprijs = $item['vanprijs'];
                    $voorprijs = $item['voorprijs'];
                } else {

                    $active = 1;
                    $vanprijs = $item_div->find('span[class=""]', 0);
                    $vanprijs = $vanprijs ?? 0;
                    $is_offer = 0;
                    if($vanprijs !== 0){
                        $is_offer = 1;
                        $vanprijs = $item_div->find('span[class=""]', 0)->plaintext;
                        $vanprijs = $this->stripChars($vanprijs);
                    }
                }
            }

            $item['scraper_vanprijs'] = $vanprijs;
            $item['scraper_voorprijs'] = $voorprijs;
            $item['scraper_is_offer'] = $is_offer;
            $item['scraper_active'] = $active;
            $this->insert($item);

            $status = $this->setResultStatus($item);
            $item['status_type'] = $status['status_type'];
            $item['status_kleur'] = $status['status_kleur'];
            $item['page'] = $status['page'];
            $item['shop'] = $this->shopName;
            $scraperResults[] = $item;
        }

        return $this->showResults($scraperResults);
    }

    public function checkNewProducts(){

    }
}
