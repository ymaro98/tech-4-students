<?php

namespace App\Http\Scrapers;

use App\Http\Controllers\ScrapeController;
use Goutte\Client;
use Illuminate\Support\Str;

class paradigit_scraper extends ScrapeController
{
    protected $shopName;
    protected $pageNumber;

    public function __construct()
    {
        $this->shopName = 'paradigit';
        $this->pageNumber = 1;
    }

    public function run()
    {
        $products = $this->getProductsByShopSlug($this->shopName);

        $scraperResults = [];
        foreach ($products as $item) {

            $url = $item->url;
            $html = $this->scrapeStore($url);
            $item_div = $html->filter('div[class="col-md-3 col-sm-5 productdetail_col_info column"]', 0);
            if(!$item_div){

                $item = $this->setInactive($item);
                $active = $item['active'];
                $is_offer = $item['is_offer'];
                $vanprijs = $item['vanprijs'];
                $voorprijs = $item['voorprijs'];
            } else {

                $voorprijs = $item_div->filter(".productdetail_product_inclvat")->count()
                    ? $item_div->filter(".productdetail_product_inclvat")->text()
                    : 0
                ;
                $voorprijs = $this->stripChars($voorprijs);
                if($voorprijs > 0 === false){

                    $item = $this->setInactive($item);
                    $active = $item['active'];
                    $is_offer = $item['is_offer'];
                    $vanprijs = $item['vanprijs'];
                    $voorprijs = $item['voorprijs'];
                } else {

                    $active = 1;
                    $vanprijs_check = $item_div->filter(".productdetail_product_advice")->text();
                    $vanprijs_check = preg_replace("/[^\d,-\.]/", "0", $vanprijs_check);
                    $vanprijs_check = $this->stripChars($vanprijs_check, 'thousand');

                    $is_offer = 0;
                    if($vanprijs_check !== 0){
                        $is_offer = 1;
                        $vanprijs = $vanprijs_check;
                    }
                }
            }

            $item['scraper_vanprijs'] = $vanprijs;
            $item['scraper_voorprijs'] = $voorprijs;
            $item['scraper_is_offer'] = $is_offer;
            $item['scraper_active'] = $active;
            $this->insert($item);

            $status = $this->setResultStatus($item);
            $item['status_type'] = $status['status_type'];
            $item['status_kleur'] = $status['status_kleur'];
            $item['page'] = $status['page'];
            $item['shop'] = $this->shopName;
            $scraperResults[] = $item;
        }

        return $this->showResults($scraperResults);
    }

    public function checkNewProducts(){

        $default = ini_get('max_execution_time');
        set_time_limit(1000);

        /**
         * Define specifation names in dutch language
         */
        $spec_names = array(
            array("dutch" => "Processorfamilie:", "dbColumns" => "cpu_fam"),
            array("dutch" => "Processor model:", "dbColumns" => "cpu_model"),
            array("dutch" => "Processor-kloksnelheid:", "dbColumns" => "cpu_ghz"),
            array("dutch" => "Type besturingssysteem:", "dbColumns" => "system"),
            array("dutch" => "Resolutie:", "dbColumns" => "resolution"),
            array("dutch" => "Schermdiagonaal:", "dbColumns" => "screen_size"),
            array("dutch" => "Videokaart model:", "dbColumns" => "gpu_mode_ddc"),
            array("dutch" => "Intern geheugen:", "dbColumns" => "ram"),
            array("dutch" => "Totale opslagcapaciteit:", "dbColumns" => "storage_size"),
            array("dutch" => "Kleur van het product:", "dbColumns" => "color"),
            array("dutch" => "Merk:", "dbColumns" => "brand"),
            array("dutch" => "Grafische processor:", "dbColumns" => "gpu_model")
        );

        $client = new Client();
        $crawler = $client->request('GET', 'https://www.paradigit.nl/laptops/laptops/page/' . $this->pageNumber);
        $links = $crawler->filter('h3>a[itemprop="url"]')->links();
        $array_of_status = array();

        foreach ($links as $link) {

            //grabbing data
            $link_page = $client->click($link);

            $productName  = $link_page->filter(".productDetail_title>h1>span")->text();
            $nameSLug = Str::slug($productName, "-");

            $list_price = $link_page->filter(".productdetail_product_advice")->text();
            $list_price = preg_replace("/[^\d,-\.]/", "0", $list_price);
            $price = array("Current Price:" => $link_page->filter(".productdetail_product_inclvat")->text(), "List Price:" => $list_price);

            $url = $link->getUri();

            $imageLink = $link_page->filter("#mainproductimage")->image();
            $imagePath = $this->createImage($imageLink->getUri(), $productName);

            $specs = array(
                "name" => $productName,
                "name_slug" => $nameSLug,
                "webshop" => $this->shopName,
                "image" => $imagePath,
                "price" => json_encode($price),
                "url" => $url
            );

            $link_page->filter("div>.row> .col-xs-6")->each(function ($node) use (&$spec_names, &$specs) {
                $nodeKey = trim(preg_replace('@<(\w+)\b.*?>.*?</\1>@si', '', $node->html()));
                if (in_array($nodeKey, array_column($spec_names, "dutch"))) {
                    $spec_name_key = array_search($nodeKey, array_column($spec_names, 'dutch'));

                    $childNode = $this->getNextFiltered($node, 'div');
                    $specs[$spec_names[$spec_name_key]["dbColumns"]] = $childNode->text();
                }
            });

            if(isset($specs['cpu_model'])){
                $cpu_model =  $specs['cpu_fam']. ' ' . $specs['cpu_model'];
            } else {
                $cpu_model =  $specs['cpu_fam'];
            }
            $specs['cpu_model'] = $cpu_model;

            if(isset($specs['gpu_mode_ddc'])){
                $gpu_model = $specs['gpu_mode_ddc'];
            } else {
                $gpu_model = $specs['gpu_model'];
            }
            $specs['gpu_model'] = $gpu_model;

            $dbStatus = $this->saveApproveItem($specs);;
            $array_of_status[$productName] = $dbStatus;
        }

        set_time_limit($default);

        if($array_of_status){

            $this->pageNumber++;
            $this->checkNewProducts();
        } else {
            return $this->scraperDashboard();
        }
    }
}
