<?php

namespace App\Http\Scrapers;

use App\Http\Controllers\ScrapeController;
use Goutte\Client;
use Illuminate\Support\Str;


class mediamarkt_scraper extends ScrapeController
{
    protected $shopName;
    protected $pageNumber;

    public function __construct()
    {
        $this->shopName = 'mediamarkt';
        $this->pageNumber = 1;
    }

    public function run()
    {
        $name = $this->shopName;
        $products = $this->getProductsByShopSlug($name);

        $scraperResults = [];
        foreach ($products as $item) {

            $url = $item->url;
            $html = $this->scrapeStore($url);
            $item_div = $html->filter('div[id="product-details"]', 0);
            if(!$item_div){

                $item = $this->setInactive($item);
                $active = $item['active'];
                $is_offer = $item['is_offer'];
                $vanprijs = $item['vanprijs'];
                $voorprijs = $item['voorprijs'];
            } else {

                $voorprijs = $item_div->filter("meta[itemprop='price']")->count()
                    ? $item_div->filter("meta[itemprop='price']")->attr("content")
                    : 0
                ;
                $voorprijs = $this->stripChars($voorprijs);
                if($voorprijs > 0 === false){

                    $item = $this->setInactive($item);
                    $active = $item['active'];
                    $is_offer = $item['is_offer'];
                    $vanprijs = $item['vanprijs'];
                    $voorprijs = $item['voorprijs'];
                } else {

                    $active = 1;
                    $vanprijs_check = $item_div->filter('div[class="old-price-block"] .price')->count() ? $item_div->filter('div[class="old-price-block"] .price')->text() : 0;
                    $vanprijs_check = $this->stripChars($vanprijs_check);

                    $is_offer = 0;
                    if($vanprijs_check !== 0){
                        $is_offer = 1;
                        $vanprijs = $vanprijs_check;
                    }
                }
            }

            $item['scraper_vanprijs'] = $vanprijs;
            $item['scraper_voorprijs'] = $voorprijs;
            $item['scraper_is_offer'] = $is_offer;
            $item['scraper_active'] = $active;
            $this->insert($item);

            $status = $this->setResultStatus($item);
            $item['status_type'] = $status['status_type'];
            $item['status_kleur'] = $status['status_kleur'];
            $item['page'] = $status['page'];
            $item['shop'] = $this->shopName;
            $scraperResults[] = $item;
        }

        return $this->showResults($scraperResults);
    }

    public function checkNewProducts(){

        $default = ini_get('max_execution_time');
        set_time_limit(1000);

        /**
         * Define specifation names in dutch language
         */
        $spec_names = array(
            array("dutch" => "Processor:", "dbColumns" => "cpu_model"),
            array("dutch" => "Processorsnelheid:", "dbColumns" => "cpu_freq"),
            array("dutch" => "Besturingssysteem:", "dbColumns" => "system"),
            array("dutch" => "Resolutie:", "dbColumns" => "resolution"),
            array("dutch" => "Beeldschermdiagonaal (inch):", "dbColumns" => "screen_size"),
            array("dutch" => "Fabrikant grafische kaart:", "dbColumns" => "gpu_fam"),
            array("dutch" => "Grafische kaart:", "dbColumns" => "gpu_model"),
            array("dutch" => "Intern geheugen:", "dbColumns" => "ram"),
            array("dutch" => "Totale opslagruimte:", "dbColumns" => "storage_size"),
            array("dutch" => "Kleur:", "dbColumns" => "color")
        );

        $client = new Client();
        $crawler = $client->request('GET', 'https://www.mediamarkt.nl/nl/category/_laptops-482723.html?searchParams=&sort=&view=&page=' . $this->pageNumber);
        $links = $crawler->filter('.product-wrapper>div>h2>a')->links();
        $array_of_status = array();
        foreach ($links as $link) {

            //grabbing data
            $link_page = $client->click($link);

            $productName  = $link_page->filter("h1[itemprop='name']")->text();
            $nameSLug = Str::slug($productName, "-");
            if($link_page->filter("img[itemprop='logo']")->count() > 0){
                $brand = $link_page->filter("img[itemprop='logo']")->attr("alt");
            } else {
                $brand = 'n/a';
            }

            $list_price = $link_page->filter('div[itemprop="offers"]')->children(".old-price-block")->count() ? $link_page->filter('div[itemprop="offers"]')->children(".old-price-block")->text() : 0;
            $list_price = preg_replace("/[^\d,\.]/", "  ", $list_price);
            $list_price = preg_replace("/\s+/", " Save ", trim($list_price));
            $price = array("Current Price" => $link_page->filter("meta[itemprop='price']")->attr("content"), "List Price:" => $list_price);

            $url = $link->getUri();
            if($link_page->filter("img[itemprop='image']")->count() > 0){

                $imageLink = $link_page->filter("img[itemprop='image']")->image();
                $imagePath = $this->createImage($imageLink->getUri() . ".png", substr($productName, 0, 10));
            } else {
                $imagePath = 'n/a';
            }

            $specs = array(
                "name" => $productName,
                "name_slug" => $nameSLug,
                "webshop" => $this->shopName,
                "brand" => $brand,
                "image" => $imagePath,
                "price" => json_encode($price),
                "url" => $url
            );

            $link_page->filter(".specification>dt")->each(function ($node) use (&$spec_names, &$specs) {
                $nodeKey = trim(preg_replace('@<(\w+)\b.*?>.*?</\1>@si', '', $node->html()));
                if (in_array($nodeKey, array_column($spec_names, "dutch"))) {
                    $spec_name_key = array_search($nodeKey, array_column($spec_names, 'dutch'));

                    $childNode = $this->getNextFiltered($node, 'dd');
                    $specs[$spec_names[$spec_name_key]["dbColumns"]] = $childNode->text();
                }
            });

            $dbStatus = $this->saveApproveItem($specs);;
            $array_of_status[$productName] = $dbStatus;
        }

        set_time_limit($default);
        // if($array_of_status){

        //     $this->pageNumber++;
        //     $this->checkNewProducts();
        // } else {
            return $this->scraperDashboard();
        // }
    }
}
