<?php

namespace App\Http\Scrapers;

use App\Http\Controllers\ScrapeController;
use Goutte\Client;
use Illuminate\Support\Str;

class alternate_scraper extends ScrapeController
{
    protected $shopName;
    protected $pageNumber;

    public function __construct()
    {
        $this->shopName = 'alternate';
        $this->pageNumber = 1;
    }

    public function run()
    {
        $products = $this->getProductsByShopSlug($this->shopName);
        $scraperResults = [];
        foreach ($products as $item) {

            $url = $item->url;
            $html = $this->scrapeStore($url);
            $item_div = $html->filter('div[class="productMainContainerTable"]', 0);
            if(!$item_div){

                $item = $this->setInactive($item);
                $active = $item['active'];
                $is_offer = $item['is_offer'];
                $vanprijs = $item['vanprijs'];
                $voorprijs = $item['voorprijs'];
            } else {
                $is_offer = 0;

                $list_price = $item_div->filter(".msrp")->count() ? $item_div->filter(".msrp")->text() : 0;
                $list_price = preg_replace("/[^\d,\.]/", " ", $list_price);

                $price = $item_div->filter("span[itemprop='price']")->count() 
                    ? $item_div->filter("span[itemprop='price']")->attr("content") 
                    : 0
                ;

                if($price != 0){
                    $temp_voorprijs = $price;
                    $temp_vanprijs = $list_price;
                } else {
                    $temp_voorprijs = $list_price;
                }

                if($temp_voorprijs > 0 === false){

                    $item = $this->setInactive($item);
                    $active = $item['active'];
                    $is_offer = $item['is_offer'];
                    $vanprijs = $item['vanprijs'];
                    $voorprijs = $item['voorprijs'];
                } else {

                    $voorprijs = $temp_voorprijs;
                    $voorprijs = $this->stripChars($voorprijs);

                    $active = 1;
                    $vanprijs = $temp_vanprijs;
                    $vanprijs = $vanprijs ?? 0;
                    $is_offer = 0;
                    if($vanprijs != 0){
                        $is_offer = 1;
                        $vanprijs = $temp_vanprijs;
                        $vanprijs = $this->stripChars($vanprijs);
                    }
                }
            }

            $item['scraper_vanprijs'] = $vanprijs;
            $item['scraper_voorprijs'] = $voorprijs;
            $item['scraper_is_offer'] = $is_offer;
            $item['scraper_active'] = $active;
            $this->insert($item);

            $status = $this->setResultStatus($item);
            $item['status_type'] = $status['status_type'];
            $item['status_kleur'] = $status['status_kleur'];
            $item['page'] = $status['page'];
            $item['shop'] = $this->shopName;
            $scraperResults[] = $item;
        }

        return $this->showResults($scraperResults);
    }

    public function checkNewProducts(){

        $default = ini_get('max_execution_time');
        set_time_limit(1000);

        /**
         * Define specifation names in dutch language
         */
        $spec_names = array(
            array("dutch" => "Naam", "dbColumns" => "cpu_model"),
            array("dutch" => "Kloksnelheid", "dbColumns" => "cpu_ghz"),
            array("dutch" => "Besturingssysteem", "dbColumns" => "system"),
            array("dutch" => "Resolutie", "dbColumns" => "resolution"),
            array("dutch" => "Grootte", "dbColumns" => "screen_size"),
            array("dutch" => "Grafisch", "dbColumns" => "gpu_model"),
            array("dutch" => "Capaciteit", "dbColumns" => "ram"),
            array("dutch" => "Totale capaciteit", "dbColumns" => "storage_size"),
            array("dutch" => "Kleur", "dbColumns" => "color"),
        );

        $client = new Client();
        $crawler = $client->request('GET', 'https://www.alternate.nl/Alle-notebooks/html/listings/1444138486045?lk=21363&page=' . $this->pageNumber . '&hideFilter=false');
        $links = $crawler->filter('.productLink')->links();
        $array_of_status = array();

        foreach ($links as $link) {

            //grabbing data
            $link_page = $client->click($link);

            $productName  = ($this->getNextFiltered($link_page->filter("span[itemprop='brand']"), 'span'))->text();
            $nameSLug = Str::slug($productName, "-");

            $brand = $link_page->filter("span[itemprop='brand']")->text();

            $list_price = $link_page->filter(".msrp")->count() ? $link_page->filter(".msrp")->text() : 0;
            $list_price = preg_replace("/[^\d,\.]/", " ", $list_price);
            if ($price = $link_page->filter("span[itemprop='price']")->count() > 0 ) {
                $price = $link_page->filter("span[itemprop='price']")->attr("content");
            } else {
                $price = 0;
            }
            $price = array("Current Price:" => $price, "List Price:" => $list_price);

            $url = $link->getUri();

            $imageLink = $link_page->filter('meta[itemprop="image"]')->attr("content");
            $imagePath = $this->createImage($imageLink, substr($productName, 0, 10));

            $specs = array(
                "name" => $productName,
                "name_slug" => $nameSLug,
                "webshop" => $this->shopName,
                "brand" => $brand,
                "image" => $imagePath,
                "price" => json_encode($price),
                "url" => $url
            );

            $link_page->filter("td")->each(function ($node) use (&$spec_names, &$specs) {
                $nodeKey = trim(preg_replace('@<(\w+)\b.*?>.*?</\1>@si', '', $node->html()));
                if (in_array($nodeKey, array_column($spec_names, "dutch"))) {
                    $spec_name_key = array_search($nodeKey, array_column($spec_names, 'dutch'));

                    if ($nodeKey == "Grafisch") {

                        $childNode =  $this->getNextFiltered($node, 'td');
                        $childNode =  $this->getNextFiltered($childNode, 'td');
                    } elseif($nodeKey == "Capaciteit"){

                        $childNode =  $this->getNextFiltered($node, 'td');
                        $childNode =  $this->getNextFiltered($childNode, 'td');
                    } else {
                        $childNode = $this->getNextFiltered($node, 'td');
                    }

                    $specs[$spec_names[$spec_name_key]["dbColumns"]] = ($childNode != null) ? $childNode->text() : 'n/a';
                }
            });

            if(!isset($specs['screen_size'])){

                $specs['screen_size'] = "n/a";
            } else {

                $temp_screen_size = $specs['screen_size'];
                $temp_screen_size = explode('(', $temp_screen_size);
                $temp_screen_size = trim(str_replace(')', '', $temp_screen_size[1]));
                $specs['screen_size'] = "$temp_screen_size";
            }

            if(!isset($specs['ram'])){

                $specs['ram'] = "n/a";
            } else {

                $temp_ram = $specs['ram'];
                $temp_ram = explode('GB', $temp_ram);
                $temp_ram = trim($temp_ram[0]);
                $specs['ram'] = "$temp_ram GB";
            }

            if(!isset($specs['resolution'])){

                $specs['resolution'] = "n/a";
            } else {
                $temp_resolution = $specs['resolution'];
                $temp_resolution = str_replace('.', '', $temp_resolution);
                $specs['resolution'] = "$temp_resolution";
            }

            $dbStatus = $this->saveApproveItem($specs);;
            $array_of_status[$productName] = $dbStatus;
        }

        set_time_limit($default);
        if($array_of_status){

            $this->pageNumber++;
            $this->checkNewProducts();
        } else {      
            return $this->scraperDashboard();
        }
    }
}
