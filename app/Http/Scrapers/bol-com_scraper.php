<?php
namespace App\Http\Scrapers;

ini_set('max_execution_time', 0);

use App\ApproveProduct;
use App\Http\Controllers\BolOpenAPI;
use App\Http\Controllers\ScrapeController;
use Illuminate\Support\Str;

class bol_com_scraper extends ScrapeController
{

    public $api;
    protected $shopName;
    protected $offsetParam;

    public function __construct()
    {
        $this->api = new BolOpenAPI();
        $this->shopName = 'bol-com';
        $this->offsetParam = 0;
        $this->aff_start = 'https://partner.bol.com/click/click?p=2&t=url&s=1097077&f=TXL&url=https%3A%2F%2Fwww.bol.com%2Fnl%2Fp%2F';
        return $this->api;
    }

    public function run()
    {

        $products = $this->getProductsByShopSlug($this->shopName);

        $scraperResults = [];
        foreach ($products as $item) {

            $url = $item->url;

            $feed = $this->api->getProduct($url);
            if(!$feed){

                $item = $this->setInactive($item);
                $active = $item['active'];
                $is_offer = $item['is_offer'];
                $vanprijs = $item['vanprijs'];
                $voorprijs = $item['voorprijs'];
            } else {

                $baseItem = $feed->offerData;
                $offers = $baseItem->offers[0] ?? false;
                if(!$offers){

                    $item = $this->setInactive($item);
                    $active = $item['active'];
                    $is_offer = $item['is_offer'];
                    $vanprijs = $item['vanprijs'];
                    $voorprijs = $item['voorprijs'];
                } else {

                    $voorprijs = $offers->price;
                    $voorprijs = $this->stripChars($voorprijs);
                    if(!$voorprijs){

                        $item = $this->setInactive($item);
                        $active = $item['active'];
                        $is_offer = $item['is_offer'];
                        $vanprijs = $item['vanprijs'];
                        $voorprijs = $item['voorprijs'];
                    } else {

                        $active = 1;
                        $vanprijs = $offers->listPrice ?? 0;
                        $is_offer = 0;
                        if($vanprijs !== 0){
                            $is_offer = 1;
                            $vanprijs = $this->stripChars($vanprijs);
                        }
                    }
                }
            }

            if(!$item->affiliate_url && stripos($url, 'https://partner.bol.com/') !== true){

                $url_prod_string = explode('https://www.bol.com/nl/p/', $url);
                $url_prod_string = $url_prod_string[1];
                $aff_click_name = rawurlencode($feed->title);

                $affiliate_url = $this->aff_start . $url_prod_string. '%2F&name='. $aff_click_name;
            }

            $item['scraper_vanprijs'] = $vanprijs;
            $item['scraper_voorprijs'] = $voorprijs;
            $item['scraper_is_offer'] = $is_offer;
            $item['scraper_active'] = $active;
            $item['affiliate_url'] = $affiliate_url;
            $this->insert($item);

            $status = $this->setResultStatus($item);
            $item['status_type'] = $status['status_type'];
            $item['status_kleur'] = $status['status_kleur'];
            $item['page'] = $status['page'];
            $item['shop'] = $this->shopName;
            $scraperResults[] = $item;
        }

        return $this->showResults($scraperResults);
    }

    public function checkNewProducts(){

        $bolApi = new BolOpenAPI();
        
        $catIdParam = 4770;
        $offersParam = 'bolcom';

        $products = $bolApi->getProductsList($catIdParam, $offersParam, $this->offsetParam);
        foreach($products as $product){

            $productName = $product->title;
            $specs = $product->attributeGroups;
            $temp = new ApproveProduct;

            $checkProduct = $temp::where('name', $productName)->first();
            if($checkProduct === null){

                foreach($specs as $spec){

                    if($spec->title == 'Software'){

                        $systemSpecs = $spec->attributes;
                        foreach($systemSpecs as $spec){

                            $issetKey = $spec->key ?? 0;
                            if($issetKey !== 0){

                                if($spec->key == 'BESTURINGSSYSTEEM'){

                                    $systemName = $spec->value;
                                }
                            }
                        }

                    } else if($spec->title == 'Processor'){

                        $cpuSpecs = $spec->attributes;
                        foreach($cpuSpecs as $spec){

                            $issetKey = $spec->key ?? 0;
                            if($issetKey !== 0){

                                if($spec->key == 'PROCESSOR_TYPE'){

                                    $cpuModelName = $spec->value;
                                    
                                } else if($spec->key == 'PROCESSOR_UITVOERING'){

                                    $cpuModelName .= ' '.$spec->value;

                                } else if($spec->key == 'KLOKSNELHEID'){

                                    $cpuFreqName = $spec->value;
                                }
                            }
                        }
                        $cpuModelName = str_replace(')','',$cpuModelName);
                        $cpuFreqName = str_replace(')','',$cpuFreqName);

                    } else if($spec->title == 'Beeldscherm'){

                        $screenSpecs = $spec->attributes;
                        foreach($screenSpecs as $spec){

                            $issetKey = $spec->key ?? 0;
                            if($issetKey !== 0){

                                if($spec->key == 'DISPLAY_RESOLUTION'){

                                    $resolutionName = $spec->value;

                                } else if($spec->key == 'BEELDSCHERM_GROOTTE'){

                                    $screenSizeName = $spec->value;
                                }
                            }
                        }

                    } else if($spec->title == 'Videokaart'){

                        $videoCardSpecs = $spec->attributes;
                        foreach($videoCardSpecs as $spec){

                            $issetKey = $spec->key ?? 0;
                            if($issetKey !== 0){

                                if($spec->key == 'VIDEOKAART_TYPE'){

                                    $gpuModelName = $spec->value;
                                }
                            }
                        }

                    } else if($spec->title == 'Geheugen'){

                        $ramSpecs = $spec->attributes;
                        foreach($ramSpecs as $spec){

                            $issetKey = $spec->key ?? 0;
                            if($issetKey !== 0){

                                if($spec->key == 'INTERN_GEHEUGEN'){

                                    $ramName = $spec->value;
                                }
                            }
                        }

                    } else if($spec->title == 'Opslag'){

                        $storageSpecs = $spec->attributes;
                        $storageName = '';
                        $storageCapString = '';
                        $storageHDDStringCheck = false;
                        $storageHDDString = '';
                        $storageSSDStringCheck = false;
                        $storageSSDString = '';

                        foreach($storageSpecs as $spec){

                            if($spec->label == 'Totale opslagcapaciteit'){

                                $storageCap = $spec->value;
                                $storageCapString = $storageCap;

                            } else if($spec->label == 'Opslagcapaciteit SSD/Flash'){

                                $storageSSD = $spec->value;
                                $storageSSDString = $storageSSD;
                                if($storageSSD != '0 GB'){

                                    $storageSSDStringCheck = true;
                                }

                            } else if($spec->label == 'Opslagcapaciteit HDD'){

                                $storageHDD = $spec->value;
                                $storageHDDString = $storageHDD;
                                if($storageHDD != '0 GB'){

                                    $storageHDDStringCheck = true;
                                }
                            }
                        }

                        $storageName = "$storageCapString ";

                        if($storageHDDStringCheck === false && $storageSSDStringCheck === true){

                            $storageName .= "(SSD)";
                        } else if($storageHDDStringCheck === true && $storageSSDStringCheck === false){

                            $storageName .= "(HDD)";
                        } else if($storageHDDStringCheck === true && $storageSSDStringCheck === true){

                            $storageName .= "HDD($storageHDDString) + SSD($storageSSDString)";
                        }

                        $storageName = $storageName;

                    } else if($spec->title == 'Overige eigenschappen'){

                        $extraSpecs = $spec->attributes;
                        foreach($extraSpecs as $spec){

                            if($spec->key == 'COLOUR'){

                                $colorName = $spec->value;
                            }
                        }
                    }
                }

                $brandName = $product->specsTag;

                if(isset($product->images)){
                    $imageLink = last($product->images)->url;
                    $image = $this->createImage($imageLink, substr($productName, 0, 10));
                } else {
                    $image = NULL;
                }
                $productLink = reset($product->urls)->value;

                $temp->name = $productName;
                $temp->name_slug = Str::slug($productName, "-");
                $temp->webshop = $this->shopName;
                $temp->brand = $brandName ?? 'Niet gevonden';
                $temp->system = $systemName ?? 'Niet gevonden';
                $temp->cpu_fam = $cpuFamName ?? 'Niet gevonden';
                $temp->cpu_model = $cpuModelName ?? 'Niet gevonden';
                $temp->cpu_ghz = $cpuFreqName ?? 'Niet gevonden';
                $temp->gpu_fam = $gpuFamName ?? 'Niet gevonden';
                $temp->gpu_model = $gpuModelName ?? 'Niet gevonden';
                $temp->resolution = $resolutionName ?? 'Niet gevonden';
                $temp->screen_size = $screenSizeName ?? 'Niet gevonden';
                $temp->ram = $ramName ?? 'Niet gevonden';
                $temp->storage_size = $storageName ?? 'Niet gevonden';
                $temp->color = $colorName ?? 'Niet gevonden';
                $temp->image = $image ?? 'Niet gevonden';
                $temp->url = $productLink;
                $temp->approved = false;
                
                $data = $temp->toArray();
                $this->saveApproveItem($data);
            }
        }

        $productsCount = count($products);
        if($productsCount > 0){
            $this->offsetParam = $this->offsetParam + 100;
            $this->checkNewProducts();
        } else {
            return $this->scraperDashboard();
        }
    }
}
