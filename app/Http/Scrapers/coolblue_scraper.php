<?php

namespace App\Http\Scrapers;

use App\Http\Controllers\ScrapeController;
use Goutte\Client;
use Illuminate\Support\Str;

class coolblue_scraper extends ScrapeController
{
    protected $shopName;
    protected $pageNumber;

    public function __construct()
    {
        $this->shopName = 'coolblue';
        $this->pageNumber = 1;
    }

    public function run()
    {

        $products = $this->getProductsByShopSlug($this->shopName);
        $scraperResults = [];
        foreach ($products as $item) {

            $url = $item->url;
            $html = $this->scrapeStore($url);
            $item_div = $html->filter('div[class="product-order"]', 0);
            if(!$item_div){

                $item = $this->setInactive($item);
                $active = $item['active'];
                $is_offer = $item['is_offer'];
                $vanprijs = $item['vanprijs'];
                $voorprijs = $item['voorprijs'];
            } else {

                $voorprijs = $item_div->filter(".js-sales-price>.sales-price__current")->count()
                    ? $item_div->filter(".js-sales-price>.sales-price__current")->text()
                    : 0
                ;
                $voorprijs = $this->stripChars($voorprijs);

                if($voorprijs > 0 === false){

                    $item = $this->setInactive($item);
                    $active = $item['active'];
                    $is_offer = $item['is_offer'];
                    $vanprijs = $item['vanprijs'];
                    $voorprijs = $item['voorprijs'];
                } else {

                    $active = 1;
                    $vanprijs_check = $item_div->filter("span.sales-price__former-price")->count() ? $item_div->filter("span.sales-price__former-price")->text() : 0;
                    $vanprijs_check = $this->stripChars($vanprijs_check);

                    $is_offer = 0;
                    if($vanprijs_check !== 0){
                        $is_offer = 1;
                        $vanprijs = $vanprijs_check;
                    }
                }
            }

            $item['scraper_vanprijs'] = $vanprijs;
            $item['scraper_voorprijs'] = $voorprijs;
            $item['scraper_is_offer'] = $is_offer;
            $item['scraper_active'] = $active;
            $this->insert($item);

            $status = $this->setResultStatus($item);
            $item['status_type'] = $status['status_type'];
            $item['status_kleur'] = $status['status_kleur'];
            $item['page'] = $status['page'];
            $item['shop'] = $this->shopName;
            $scraperResults[] = $item;
        }

        return $this->showResults($scraperResults);
    }

    public function checkNewProducts(){

        $default = ini_get('max_execution_time');
        set_time_limit(1000);

        /**
         * Define specifation names in dutch language
         */
        $spec_names = array(
            array("dutch" => "Processor", "dbColumns" => "cpu_fam"),
            array("dutch" => "Processor-nummer", "dbColumns" => "cpu_model"),
            array("dutch" => "Turbo snelheid", "dbColumns" => "cpu_ghz"),
            array("dutch" => "Besturingssysteem", "dbColumns" => "system"),
            array("dutch" => "Scherpte", "dbColumns" => "resolution"),
            array("dutch" => "Schermdiagonaal", "dbColumns" => "screen_size"),
            array("dutch" => "Videokaart chipset", "dbColumns" => "gpu_model"),
            array("dutch" => "Intern werkgeheugen (RAM)", "dbColumns" => "ram"),
            array("dutch" => "Totale opslagcapaciteit", "dbColumns" => "storage_size"),
            array("dutch" => "Kleur", "dbColumns" => "color")
        );

        $client = new Client();
        $crawler = $client->request('GET', 'https://www.coolblue.nl/laptops?pagina=' . $this->pageNumber);
        $links = $crawler->filter('.product-card__title>.link')->links();
        $array_of_status = array();

        foreach ($links as $link) {

            //grabbing data
            $link_page = $client->click($link);

            $productName  = $link_page->filter(".js-product-name")->text();
            $nameSLug = Str::slug($productName, "-");

            $brand = explode(" ", $productName);
            $brand = $brand[0];

            $list_price = $link_page->filter("span.sales-price__former-price")->count() ? $link_page->filter("span.sales-price__former-price")->text() : 0;
            $price = array(
                "Current Price:" => $link_page->filter(".js-sales-price>.sales-price__current")->text(),
                "List Price:" => $list_price
            );

            $url = $link->getUri();
            if($link_page->filter(".product-media-gallery__item-image")->count() > 0){
                $imageLink = $link_page->filter(".product-media-gallery__item-image")->image();
                $imagePath = $this->createImage($imageLink->getUri(), substr($productName, 0, 10));
            } else {
                $imagePath = 'n/a';
            }

            $specs = array(
                "name" => $productName,
                "name_slug" => $nameSLug,
                "webshop" => $this->shopName,
                "brand" => $brand,
                "image" => $imagePath,
                "price" => json_encode($price),
                "url" => $url
            );

            $link_page->filter(".product-specs__help-title")->each(function ($node) use (&$spec_names, &$specs) {

                $nodeKey = trim($node->text());
                $node = $node->parents();

                if (in_array($nodeKey, array_column($spec_names, "dutch"))) {
                    $spec_name_key = array_search($nodeKey, array_column($spec_names, 'dutch'));

                    $childNode = $this->getNextFiltered($node, 'dd');
                    $specs[$spec_names[$spec_name_key]["dbColumns"]] = $childNode->text();
                }
            });

            if(isset($specs['cpu_model'])){
                $cpu_model =  $specs['cpu_fam']. ' ' . $specs['cpu_model'];
            } else {
                $cpu_model =  $specs['cpu_fam'];
            }
            $specs['cpu_model'] = $cpu_model;
            
            $dbStatus = $this->saveApproveItem($specs);;
            $array_of_status[$productName] = $dbStatus;
        }

        set_time_limit($default);
        if($array_of_status){

            $this->pageNumber++;
            $this->checkNewProducts();
        } else {
            return $this->scraperDashboard();
        }
    }
}
