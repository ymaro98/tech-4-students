<?php
namespace App\Http\Scrapers;

ini_set('max_execution_time', 0);

use App\ApproveProduct;
use Goutte\Client;
use App\Http\Controllers\ScrapeController;
use Illuminate\Support\Str;


class bcc_scraper extends ScrapeController
{

    protected $shopName;
    protected $pageNumber;

    public function __construct()
    {
        $this->shopName = 'bcc';
        $this->pageNumber = 1;
    }

    public function run()
    {
        $products = $this->getProductsByShopSlug($this->shopName);

        $scraperResults = [];
        foreach ($products as $item) {

            $url = $item->url;
            $html = $this->scrapeStore($url);
            $item_div = $html->filter('section[class="productoffer"]', 0);
            if(!$item_div){

                $item = $this->setInactive($item);
                $active = $item['active'];
                $is_offer = $item['is_offer'];
                $vanprijs = $item['vanprijs'];
                $voorprijs = $item['voorprijs'];
            } else {

                $voorprijs = $item_div->filter('span[class="priceblock__price priceblock__price--salesprice priceblock__price--FloatingDecimal"]', 0)->count()
                    ? $item_div->filter('span[class="priceblock__price priceblock__price--salesprice priceblock__price--FloatingDecimal"]', 0)->text()
                    : 0
                ;
                $voorprijs = $this->stripChars($voorprijs);

                if($voorprijs > 0 === false){

                    $item = $this->setInactive($item);
                    $active = $item['active'];
                    $is_offer = $item['is_offer'];
                    $vanprijs = $item['vanprijs'];
                    $voorprijs = $item['voorprijs'];
                } else {

                    $active = 1;
                    $vanprijs_check = $item_div->filter('section.productoffer span.priceblock__price', 0)->text();
                    $vanprijs_check = $this->stripChars($vanprijs_check);
                    if(!$vanprijs_check || $vanprijs_check == $voorprijs){

                        $is_offer = 0;
                        $vanprijs = 0;
                    } else {
                        $is_offer = 1;
                        $vanprijs = $vanprijs_check;
                    }
                }
            }

            $item['scraper_vanprijs'] = $vanprijs;
            $item['scraper_voorprijs'] = $voorprijs;
            $item['scraper_is_offer'] = $is_offer;
            $item['scraper_active'] = $active;
            $this->insert($item);

            $status = $this->setResultStatus($item);
            $item['status_type'] = $status['status_type'];
            $item['status_kleur'] = $status['status_kleur'];
            $item['page'] = $status['page'];
            $item['shop'] = $this->shopName;
            $scraperResults[] = $item;
        }
        
        return $this->showResults($scraperResults);
    }

    public function checkNewProducts(){

        $default = ini_get('max_execution_time');
        set_time_limit(1000);

        /**
         * Define specifation names in dutch language
         */
        $spec_names = array(
            array("dutch" => "Webshop", "dbColumns" => "webshop"),
            array("dutch" => "Processorfamilie", "dbColumns" => "cpu_fam"),
            array("dutch" => "Processormodel", "dbColumns" => "cpu_model"),
            array("dutch" => "Processor frequentie", "dbColumns" => "cpu_ghz"),
            array("dutch" => "Besturingssysteem", "dbColumns" => "system"),
            array("dutch" => "Resolutie", "dbColumns" => "resolution"),
            array("dutch" => "Beeldschermdiagonaal", "dbColumns" => "screen_size"),
            array("dutch" => "On-board grafisch adapter model", "dbColumns" => "gpu_model"),
            array("dutch" => "Intern geheugen", "dbColumns" => "ram"),
            array("dutch" => "Totale opslagcapaciteit", "dbColumns" => "storage_size"),
            array("dutch" => "Kleur van het product", "dbColumns" => "color"),

        );

        $client = new Client();
        $crawler = $client->request('GET', 'https://www.bcc.nl/computer/laptop?index=' . (($this->pageNumber - 1) * 18));
        $links = $crawler->filter('.lister-product__titlelink')->links();
        $array_of_status = array();

        foreach ($links as $link) {

            $link_page = $client->click($link);

            $productName  = $link_page->filter(".pdp__title")->text();
            $nameSLug = Str::slug($productName, "-");

            $altBrandName = explode(" ", $productName);
            $brand = $link_page->filter(".brandlogo__image")->count() ? $link_page->filter(".brandlogo__image")->attr("alt") : $altBrandName[0];

            $list_price = $link_page->filter(".priceblock__price--listprice")->count() ? $link_page->filter(".priceblock__price--listprice")->text() : 0;
            $list_price = preg_replace("/[^\d,\.]/", " ", $list_price);
            $price = $link_page->filter(".priceblock__price--salesprice")->html();
            $price = trim(preg_replace('@<(\w+)\b.*?>.*?</\1>@si', '', $price));
            $price = array("Current Price:" => $price, "List Price:" => $list_price);

            $url = $link->getUri();

            $imageLink = $link_page->filter('img')->eq(0)->image();
            $imagePath = $this->createImage($imageLink->getUri(), $productName);

            $specs = array(
                "name" => $productName,
                "name_slug" => $nameSLug,
                "webshop" => $this->shopName,
                "brand" => $brand,
                "image" => $imagePath,
                "price" => json_encode($price),
                "url" => $url
            );

            $link_page->filter(".productspecifications__spec-name")->each(function ($node) use (&$spec_names, &$specs) {
                $nodeKey = trim(preg_replace('@<(\w+)\b.*?>.*?</\1>@si', '', $node->html()));
                if (in_array($nodeKey, array_column($spec_names, "dutch"))) {
                    $spec_name_key = array_search($nodeKey, array_column($spec_names, 'dutch'));

                    $childNode = $this->getNextFiltered($node, 'td');
                    $specs[$spec_names[$spec_name_key]["dbColumns"]] = $childNode->text();
                }
            });
            
            $dbStatus = $this->saveApproveItem($specs);
            $array_of_status[$productName] = $dbStatus;
        }

        set_time_limit($default);

        if($array_of_status){

            $this->pageNumber++;
            $this->checkNewProducts();
        } else {
            return $this->scraperDashboard();
        }
    }
}
