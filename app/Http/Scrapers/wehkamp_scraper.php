<?php

namespace App\Http\Scrapers;

ini_set('max_execution_time', 0);

use App\Http\Controllers\ScrapeController;
use Goutte\Client;
use Illuminate\Support\Str;
use Nesk\Puphpeteer\Puppeteer;
use Nesk\Rialto\Data\JsFunction;


class wehkamp_scraper extends ScrapeController
{
    protected $shopName;
    protected $pageNumber;

    public function __construct()
    {
        $this->shopName = 'wehkamp';
    }

    public function run()
    {

        $products = $this->getProductsByShopSlug($this->shopName);
        $scraperResults = [];
        foreach ($products as $item) {

            $url = $item->url;
            $html = $this->scrapeStore($url);
            $item_div = $html->filter('div[class="PdpDesktop__pdpd-container___3THhz position-relative full-width full-height"]', 0);
            if(!$item_div){

                $item = $this->setInactive($item);
                $active = $item['active'];
                $is_offer = $item['is_offer'];
                $vanprijs = $item['vanprijs'];
                $voorprijs = $item['voorprijs'];
            } else {

                $voorprijs = $item_div->filter("div.PricingInfo__ba-pricing___3JnR4 span.font-weight-bold")->count()
                    ? $item_div->filter("div.PricingInfo__ba-pricing___3JnR4 span.font-weight-bold")->text()
                    : 0 
                ;
                $voorprijs = $this->stripChars($voorprijs);
                if($voorprijs > 0 === false){

                    $item = $this->setInactive($item);
                    $active = $item['active'];
                    $is_offer = $item['is_offer'];
                    $vanprijs = $item['vanprijs'];
                    $voorprijs = $item['voorprijs'];
                } else {

                    $active = 1;
                    $vanprijs_check = $item_div->filter(".PricingInfo__ba-pricing___3JnR4>span>span.UI_Currency_scratch")->count() ? $item_div->filter(".PricingInfo__ba-pricing___3JnR4>span>span.UI_Currency_scratch")->text() : 0;
                    $vanprijs_check = $this->stripChars($vanprijs_check);

                    $is_offer = 0;
                    if($vanprijs_check !== 0){
                        $is_offer = 1;
                        $vanprijs = $vanprijs_check;
                    }
                }
            }

            $item['scraper_vanprijs'] = $vanprijs;
            $item['scraper_voorprijs'] = $voorprijs;
            $item['scraper_is_offer'] = $is_offer;
            $item['scraper_active'] = $active;
            $this->insert($item);

            $status = $this->setResultStatus($item);
            $item['status_type'] = $status['status_type'];
            $item['status_kleur'] = $status['status_kleur'];
            $item['page'] = $status['page'];
            $item['shop'] = $this->shopName;
            $scraperResults[] = $item;
        }

        return $this->showResults($scraperResults);
    }

    public function checkNewProducts(){

        $default = ini_get('max_execution_time');
        set_time_limit(1000);

        /**
         * Define specifation names in dutch language
         */
        $spec_names = array(
            array("dutch" => "type processor", "dbColumns" => "cpu_fam"),
            array("dutch" => "processor", "dbColumns" => "cpu_model"),
            array("dutch" => "frequentie van processor (ghz)", "dbColumns" => "cpu_ghz"),
            array("dutch" => "besturingssysteem", "dbColumns" => "system"),
            array("dutch" => "beeldschermresolutie (pixels)", "dbColumns" => "resolution"),
            array("dutch" => "beeldschermgrootte (inch)", "dbColumns" => "screen_size"),
            array("dutch" => "dedicated videokaart", "dbColumns" => "gpu_model_ddc"),
            array("dutch" => "on-board videokaart", "dbColumns" => "gpu_model"),
            array("dutch" => "intern geheugen (gb)", "dbColumns" => "ram"),
            array("dutch" => "totale opslagcapaciteit (gb)", "dbColumns" => "storage_size"),
            array("dutch" => "kleur", "dbColumns" => "color"),
            array("dutch" => "merk", "dbColumns" => "brand"),
            array("dutch" => "fabriekstype", "dbColumns" => "name")
        );

        $client = new Client();
        $crawler = $client->request('GET', 'https://www.wehkamp.nl/elektronica/laptops/C26_6F4/?PI=' . $this->pageNumber);
        $links = $crawler->filter('article>.UI_ProductTile_tile')->links();
        $array_of_status = array();

        foreach ($links as $link) {

            //grabbing data
            $link_page = $client->click($link);
            
            if($link_page->filter(".ProductDescription__pipd-heading___2A2uZ")->count()){
                $productName  = $link_page->filter(".ProductDescription__pipd-heading___2A2uZ")->text();
                $nameSLug = Str::slug($productName, "-");
            } else {
                $productName  = 'n/a';
                $nameSLug = 'n/a';
            }

            $price = array(
                "Current Price:" => $link_page->filter(".PricingInfo__ba-pricing___3JnR4>span>span")->text(),
                "List Price:" => 0
            );

            $url = $link->getUri();

            $imageLink = $link_page->filter(".Image__contain___sYes1")->image();
            $imagePath = $this->createImage($imageLink->getUri(), substr($productName, 0, 10));

            $specs = array(
                "name" => $productName,
                "name_slug" => $nameSLug,
                "webshop" => $this->shopName,
                "image" => $imagePath,
                "price" => json_encode($price),
                "url" => $url
            );

            $puppeteer = new Puppeteer;
            $browser = $puppeteer->launch([
                'args' => [
                    '--no-sandbox',
                    '--disable-setuid-sandbox',
                ]
            ]);

            $page = $browser->newPage();
            $page->goto(
                $url,
                [
                    'timeout' => 1000000, // In milliseconds
                ]
            );

            // Get the "viewport" of the page, as reported by the page.
            $dimensions = $page->evaluate(JsFunction::createWithBody("
                return {
                    json: window.__REDUX_STATE__
                };
            "));

            $bullets = $dimensions["json"]["product"]["activeProduct"]["buyingArea"]["bullets"];
            $bullets = array_map(function ($x) {
                return explode(":", strtolower($x));
            }, $bullets);

            $browser->close();
            foreach ($spec_names as $sname) {
                $search = array_search($sname["dutch"], array_column($bullets, "0"));
                if ($search) {
                    $specs[$sname["dbColumns"]] = trim($bullets[$search][1]);
                }
            }

            if(isset($specs['gpu_model_ddc']) && $specs['gpu_model_ddc'] != 'niet beschikbaar' && $specs['gpu_model_ddc'] != 'nvt' && $specs['gpu_model_ddc'] != 'nee'){
                $gpu_model = $specs['gpu_model_ddc'];
            } else {
                $gpu_model = $specs['gpu_model'] ?? 'n/a';
            }

            if(stripos($gpu_model, 'intel') !== false){

                $gpu_model = explode('intel', $gpu_model);
                $gpu_model = explode(' ', $gpu_model[1]);
                $gpu_model[0] = 'Intel';
                $gpu_model = implode(' ', $gpu_model);
            } elseif(stripos($gpu_model, 'nvidia') !== false) {
                $gpu_model = explode('geforce', $gpu_model);

                if(isset($gpu_model[1])){
                    $index = 1;
                } else {
                    $index = 0;
                }   

                $gpu_model = explode(' ', $gpu_model[$index]);
                $gpu_model[0] = 'NVIDIA';
                array_splice( $gpu_model, 1, 0, 'Geforce');
                $gpu_model = implode(' ', $gpu_model);
            }
            $specs['gpu_model'] = $gpu_model;
        
            if(stripos($specs['ram'], 'gb') !== true || stripos($specs['ram'], 'mb') !== true){
                if(strlen($specs['ram']) > 3){
                    $ram = $specs['ram']. ' MB';
                } else {
                    $ram = $specs['ram']. ' GB';
                }
            } else {
                $ram = $specs['ram'];
            }
            $specs['ram'] = $ram;

            if(stripos($specs['storage_size'], 'gb') !== true || stripos($specs['storage_size'], 'mb') !== true){
                $storage_size = $specs['storage_size']. ' GB';
            } else {
                $storage_size = $specs['storage_size'];
            }
            $specs['storage_size'] = $storage_size;

            $dbStatus = $this->saveApproveItem($specs);
            $array_of_status[$productName] = $dbStatus;
        }

        set_time_limit($default);
        if($array_of_status){

            $this->pageNumber++;
            $this->checkNewProducts();
        } else {
            return $this->scraperDashboard();
        }
    }
}
