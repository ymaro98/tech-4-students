<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

class Product extends Model
{
    protected $fillable = [
        'name',
        'name_slug',
        'soort_product_id',
        'brand_id',
        'system_id',
        'resolution_id',
        'screen_size_id',
        'cpu_fam_id',
        'cpu_model_id',
        'cpu_ghz_id',
        'gpu_fam_id',
        'gpu_model_id',
        'ram_id',
        'storage_size_id',
        'color_id',
        'description',
        'image',
    ];

    public function webshopProducts()
    {
        return $this->hasMany('App\WebshopProduct');
    }

    public function brand()
    {
        return $this->belongsTo('App\Brand');
    }

    public function system()
    {
        return $this->belongsTo('App\OperatingSystem');
    }

    public function resolution()
    {
        return $this->belongsTo('App\Resolution');
    }

    public function screenSize()
    {
        return $this->belongsTo('App\ScreenSize');
    }

    public function cpu_family()
    {
        return $this->belongsTo('App\CpuFamily', 'cpu_fam_id');
    }

    public function cpu_model()
    {
        return $this->belongsTo('App\CpuModel');
    }

    public function cpu_ghz()
    {
        return $this->belongsTo('App\CpuGhz');
    }

    public function gpu_family()
    {
        return $this->belongsTo('App\GpuFamily', 'gpu_fam_id');
    }

    public function gpu_model()
    {
        return $this->belongsTo('App\GpuModel');
    }

    public function ram()
    {
        return $this->belongsTo('App\Ram');
    }

    public function storage()
    {
        return $this->belongsTo('App\StorageSize');
    }

    public function color()
    {
        return $this->belongsTo('App\Color');
    }

    public function soort_product()
    {
        return $this->belongsTo('App\SoortProduct');
    }

    public function getMinShopProductPrice()
    {
        $prices = $this->webshopProducts->filter(function ($item) {
            return !is_null($item->prijs) && $item->prijs > 0;
        });

        return $prices->min('prijs');
    }

    public function getMaxShopProductPrice()
    {
        $prices = $this->webshopProducts->filter(function ($item) {
            return !is_null($item->prijs) && $item->prijs > 0;
        });

        return $prices->max('prijs');
    }

    public function getMinOffersPrice()
    {
        $offerPrices = $this->webshopProducts->filter(function ($item) {

            $price = $item->offers->filter(function ($item) {

                return !is_null($item->voorprijs) && $item->voorprijs > 0;
            });

            return $price;
        });

        $prices = new Collection;
        foreach($offerPrices as $price){
            $prices[] = $price->offers;
        }

        return $prices[0]->min('voorprijs');
    }

    public function getMaxOffersPrice()
    {
        $offerPrices = $this->webshopProducts->filter(function ($item) {

            $price = $item->offers->filter(function ($item) {

                return !is_null($item->voorprijs) && $item->voorprijs > 0;
            });

            return $price;
        });

        $prices = new Collection;
        foreach($offerPrices as $price){
            $prices[] = $price->offers;
        }

        return $prices[0]->max('voorprijs');
    }

    public function getMinPrice()
    {
        if($this->getMinShopProductPrice() == 0){
            $price = $this->getMinOffersPrice();
        } else {
            $price = $this->getMinShopProductPrice();
        }

        return $price;
    }

    public function getMaxPrice()
    {
        if($this->getMaxShopProductPrice() == 0){
            $price = $this->getMaxOffersPrice();
        } else {
            $price = $this->getMaxShopProductPrice();
        }

        return $price;
    }
}
