<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable = [
        'name',
        'name_slug',
    ];

    public function soort_products()
    {
        return $this->hasMany('App\SoortProduct');
    }
}
