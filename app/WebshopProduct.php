<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class WebshopProduct extends Model
{
    protected $fillable = [
        'webshop_id',
        'product_id',
        'prijs',
        'active',
    ];

    public function webshop()
    {
        return $this->belongsTo('App\Webshop');
    }

    public function product()
    {
        return $this->belongsTo('App\Product');
    }

    public function offers()
    {
        return $this->hasMany('App\Offer');
    }

    public function offer()
    {
        return $this->hasOne('App\Offer');
    }

    public function scopeIsActive($query)
    {
        return $query->where('active', 1);
    }

    public function scopeCheckHasOffer($query)
    {
        return $query->where('is_offer', '!=', 1);
    }
}
