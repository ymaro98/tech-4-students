<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ApproveProduct extends Model
{
    protected $fillable = [
        'name',
        'name_slug',
        'webshop',
        'brand',
        'system',
        'resolution',
        'screen_size',
        'cpu_fam',
        'cpu_model',
        'cpu_ghz',
        'gpu_fam',
        'gpu_model',
        'ram',
        'storage_size',
        'color',
        'url',
        'image',
        'approved',
    ];

    public function scopeIsNotApproved($query)
    {
        return $query->where('approved', 0);
    }

    public function scopeWhereShop($query, $shop)
    {
        return $query->where('webshop', $shop);
    }
}
