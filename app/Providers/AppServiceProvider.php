<?php

namespace App\Providers;

use App\View\Components\Footer;
use App\View\Components\SimilarOption;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {

        \Carbon\Carbon::setLocale(config('app.locale'));

        Blade::component('footer', Footer::class);
        Blade::component('similar-option', SimilarOption::class);

        Builder::macro('addSubSelect', function($column, $query){

            if(is_null($this->getQuery()->columns)){
                $this->select($this->getQuery()->from.'.*');
            }

            return $this->selectSub($query->limit(1)->getQuery(), $column);
        });

        if(request()->ip() == env('APP_HOMEADDR') || request()->ip() == '127.0.0.1'){
            config(['app.debug' => true]);
        }
    }
}
