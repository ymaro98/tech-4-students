<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    protected $fillable = [
        'title',
        'excerpt',
        'body',
        'image',
        'slug',
        'meta_description',
        'meta_keywords',
        'status',
    ];

    public static function findBySlug($slug)
    {
        return static::where('slug', $slug)
            ->where('status', 'ACTIVE')
        ;
    }
}
