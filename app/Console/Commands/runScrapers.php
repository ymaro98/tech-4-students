<?php

namespace App\Console\Commands;

use App\Http\Controllers\ScrapeController;
use Illuminate\Console\Command;

class runScrapers extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'scrapers:run';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command to run all scrapers to keep offers up to date';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $scraper = new ScrapeController();
        $scraper->runScrapers();
    }
}
