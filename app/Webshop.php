<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Webshop extends Model
{
    protected $fillable = [
        'name',
        'name_slug',
        'affiliate',
    ];

    public function webshopProducts()
    {
        return $this->hasMany('App\WebshopProduct');
    }

    public static function findShopBySlug($slug)
    {
        return static::where('name_slug', $slug);
    }

    public function scopeIsActive($query)
    {
        return $query->where('active', 1);
    }

    public function scopeWithProductsCount($query)
    {
        $query->addSubSelect('products_count', WebshopProduct::selectRaw('count(webshop_products.id)')
            ->whereRaw('webshop_products.webshop_id = webshops.id')
            ->where('webshop_products.active', 1)
        )->with('webshopProducts');
    }

    public function scopeWithOffersCount($query)
    {
        $query->addSubSelect('offers_count', Offer::selectRaw('count(offers.id)')
            ->join('webshop_products', 'webshop_products.id', '=', 'offers.webshop_product_id')
            ->whereRaw('webshop_products.webshop_id = webshops.id')
            ->where('offers.active', 1)
        )->with('webshopProducts.offers');
    }
}
