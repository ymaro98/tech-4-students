<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Color extends Model
{
    protected $fillable = [
        'name',
        'name_slug',
        'color_code',
    ];

    public function products()
    {
        return $this->hasMany('App\Product');
    }
}
