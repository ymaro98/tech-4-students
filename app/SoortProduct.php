<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SoortProduct extends Model
{
    protected $fillable = [
        'name',
        'name_slug',
        'category_id',
    ];

    public function products()
    {
        return $this->hasMany('App\Product');
    }

    public function category()
    {
        return $this->belongsTo('App\Category');
    }
}
